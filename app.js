const express = require("express");
const cors = require("cors");
//làm chức năng gửi mail
const nodemailer = require("nodemailer");
const { OAuth2Client } = require("google-auth-library");
// reset password
const crypto = require("crypto");
const fs = require("fs"); //xóa ảnh cũ sau khi upload ảnh mới
const { v4: uuidv4 } = require("uuid");
require("dotenv").config();
const db = require("./app/models");
const { initial } = require("./data");
const port = process.env.SERVER_PORT || 8000;
const app = express();
app.use(cors()); // Allow all CORS requests
const mongoose = require("mongoose");
app.use(express.static("public"));
const http = require("http");
const { createWebSocketServer } = require("./websocket"); // Import function to create WebSocket server

const productTypeRouter = require("./app/routes/productType.router");
const productRouter = require("./app/routes/product.router");
const customerRouter = require("./app/routes/customer.router");
const orderRouter = require("./app/routes/order.router");
const orderDetailRouter = require("./app/routes/orderDetail.router");
const productModel = require("./app/models/product.model");
const userRouter = require("./app/routes/user.router");
// const userDetailModel = require("./app/models/userDetail.model");

// const port = 8000;
app.use(express.json());

db.mongoose
  .connect(
    `mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`
  )
  .then(() => {
    console.log("Connect mongoDB Successfully");
    initial();
  })
  .catch((err) => {
    console.error("Connection error", err);
    process.exit();
  });

app.get("/test", (req, res) => {
  res.json({
    message: "test chạy thử",
  });
});
//upload ảnh
// Configure Multer storage
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/images");
  },
  filename: function (req, file, cb) {
    // cb(null, Date.now() + '-' + file.originalname);
    cb(null, uuidv4() + "-" + file.originalname);
  },
});

const upload = multer({ storage: storage }).array("files", 3); // Allow up to 3 files

// API to handle image upload
app.post("/upload", (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      return res.sendStatus(500);
    }
    // Assuming req.body.oldFiles contains the filenames of old images
    const oldFiles = req.body.oldFiles ? JSON.parse(req.body.oldFiles) : [];
    console.log(oldFiles);
    // Kiểm tra nếu oldFiles là một mảng hợp lệ
    if (Array.isArray(oldFiles) && oldFiles.length > 0) {
      // Delete old files
      oldFiles.forEach((file) => {
        const filePath = `public/images/${file}`;
        fs.unlink(filePath, (err) => {
          if (err) {
            console.error(`Failed to delete old file: ${filePath}`, err);
          } else {
            console.log(`Successfully deleted old file: ${filePath}`);
          }
        });
      });
    } else {
      console.log("No old files to delete.");
    }

    res.json(req.files.map((file) => ({ filename: file.filename })));
  });
});
//upload product type image
// Configure Multer storage
const storageType = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/images/types");
  },
  filename: function (req, file, cb) {
    cb(null, uuidv4() + "-" + file.originalname);
  },
});

// Configure Multer to handle specific fields
const uploadTypeImage = multer({ storage: storageType }).fields([
  { name: "imageUrl", maxCount: 1 },
  { name: "imageUrlBackground", maxCount: 1 },
]);

// API to handle image upload
app.post("/uploadType", (req, res) => {
  uploadTypeImage(req, res, (err) => {
    console.log(err);
    if (err) {
      return res.sendStatus(500);
    }

    // Assuming req.body.oldFiles contains the filenames of old images
    const oldFiles = req.body.oldFiles ? JSON.parse(req.body.oldFiles) : [];
    // Kiểm tra nếu oldFiles là một mảng hợp lệ
    if (Array.isArray(oldFiles) && oldFiles.length > 0) {
      // Delete old files
      oldFiles.forEach((file) => {
        const filePath = path.join("public/images/types", file);
        fs.unlink(filePath, (err) => {
          if (err) {
            console.error(`Failed to delete old file: ${filePath}`, err);
          } else {
            console.log(`Successfully deleted old file: ${filePath}`);
          }
        });
      });
    } else {
      console.log("No old files to delete.");
    }

    // Send back the filenames of the uploaded files
    res.json({
      imageUrl: req.files.imageUrl ? req.files.imageUrl[0].filename : null,
      imageUrlBackground: req.files.imageUrlBackground
        ? req.files.imageUrlBackground[0].filename
        : null,
    });
  });
});

// // Khai báo kết nối mongoDB qua mongoose
// mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Product_shop24h")
//     .then(() => {
//         console.log("Connect mongoDB Successfully");
//     })
//     .catch((err) => {
//         console.log(err);
//     });
app.use("/productTypes", productTypeRouter);
app.use("/products", productRouter);
app.use("/customers", customerRouter);
app.use("/orders", orderRouter);
app.use("/orderDetails", orderDetailRouter);
app.use("/users", userRouter);
// app.use("/userDetails",userDetailModel);

//get promotion product list
app.get("/promotions", async (req, res) => {
  const limit = req.query.limit;
  const page = req.query.page;
  const nameProductQuery = req.query.name;
  const minPriceQuery = req.query.min;
  const maxPriceQuery = req.query.max;
  const inStockQuery = req.query.inStock;
  const outStockQuery = req.query.outStock;
  const allStockQuery = req.query.allStock;
  var typesArray = [];
  var condition = {};
  condition.promotionPrice = { $gt: 0 };
  // var condiType = []
  if (req.query.typeId !== undefined) {
    const productTypeId = req.query.typeId;
    typesArray = productTypeId.split(",");
    condition.$or = [...typesArray.map((value) => ({ type: value }))];
  }
  if (nameProductQuery) {
    //ko phân biệt hoa thường
    condition.name = { $regex: new RegExp(nameProductQuery, "i") };
  }
  // //lấy các sp ko nằm trong danh sách xóa mềm
  // condition.deletedAt = null;
  //chuyển từ string về boolean
  const inStock = inStockQuery === "true";
  const outStock = outStockQuery === "true";
  const allStock = allStockQuery === "true";
  if (inStock && !outStock && !allStock) {
    condition.amount = { $gt: 0 }; // Chỉ đặt điều kiện cho amount khi chỉ có inStockQuery được kích hoạt
  }
  if (outStock && !inStock && !allStock) {
    condition.amount = { $lte: 0 }; // Chỉ đặt điều kiện cho amount khi chỉ có outStockQuery được kích hoạt
  }
  if (allStock || (inStock && outStock)) {
    condition.$or = [
      { amount: { $gt: 0 } }, // Chọn các mục có số lượng lớn hơn 0
      { amount: { $lte: 0 } },
    ];
  }
  if (minPriceQuery) {
    condition.buyPrice = { $gte: minPriceQuery };
  }
  if (maxPriceQuery) {
    condition.buyPrice = { ...condition.minPriceQuery, $lte: maxPriceQuery };
  }

  try {
    const result = await productModel
      .find(condition)
      .limit(limit)
      .skip((page - 1) * limit)
      .populate("type");
    return res.status(200).json({
      message: "Lấy tất cả sp thành công",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Đã xảy ra lỗi",
    });
  }
});
//Auth
app.use("/api/auth/", require("./app/routes/auth.router"));
app.use("/api/user/", require("./app/routes/user.router"));

// Tạo WebSocket server
const server = http.createServer(app);
createWebSocketServer(server); // Create WebSocket server

//api gửi email
// Khởi tạo OAuth2Client với Client ID và Client Secret
const myOAuth2Client = new OAuth2Client(
  process.env.GOOGLE_MAILER_CLIENT_ID,
  process.env.GOOGLE_MAILER_CLIENT_SECRET,
  "https://developers.google.com/oauthplayground"
);
// console.log(process.env.GOOGLE_MAILER_REFRESH_TOKEN);
// Set Refresh Token vào OAuth2Client Credentials
myOAuth2Client.setCredentials({
  refresh_token: process.env.GOOGLE_MAILER_REFRESH_TOKEN,
});
// Tạo API /email/send với method POST
app.post("/email/send", async (req, res) => {
  try {
    // Lấy thông tin gửi lên từ client qua body
    const { email, subject, content } = req.body;
    if (!email || !subject || !content)
      throw new Error("Please provide email, subject and content!");
    /**
     * Lấy AccessToken từ RefreshToken (bởi vì Access Token cứ một khoảng thời gian ngắn sẽ bị hết hạn)
     * Vì vậy mỗi lần sử dụng Access Token, chúng ta sẽ generate ra một thằng mới là chắc chắn nhất.
     */
    const myAccessTokenObject = await myOAuth2Client.getAccessToken();
    // Access Token sẽ nằm trong property 'token' trong Object mà chúng ta vừa get được ở trên
    const myAccessToken = myAccessTokenObject?.token;
    // Tạo một biến Transport từ Nodemailer với đầy đủ cấu hình, dùng để gọi hành động gửi mail
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: process.env.ADMIN_EMAIL_ADDRESS,
        clientId: process.env.GOOGLE_MAILER_CLIENT_ID,
        clientSecret: process.env.GOOGLE_MAILER_CLIENT_SECRET,
        refresh_token: process.env.GOOGLE_MAILER_REFRESH_TOKEN,
        accessToken: myAccessToken,
      },
    });
    // mailOption là những thông tin gửi từ phía client lên thông qua API
    const mailOptions = {
      to: email, // Gửi đến ai?
      subject: subject, // Tiêu đề email
      html: content, // Nội dung email
    };
    // Gọi hành động gửi email
    await transport.sendMail(mailOptions);
    // Không có lỗi gì thì trả về success
    res.status(200).json({ message: "Email sent successfully." });
  } catch (error) {
    // Có lỗi thì các bạn log ở đây cũng như gửi message lỗi về phía client
    res.status(500).json({ errors: error.message });
  }
});
server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
