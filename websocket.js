const WebSocket = require('ws');
let wss;

function createWebSocketServer(server) {
  wss = new WebSocket.Server({ server });

  wss.on('connection', function connection(ws) {
    console.log('Client connected');

    ws.on('message', function incoming(message) {
      console.log('Received message:', message);
    });

    ws.on('close', function close() {
      console.log('Client disconnected');
    });
  });
}

function notifyProductInfosChange() {
  if (wss) {
    wss.clients.forEach(function each(client) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify({ type: 'infoChange' }));
      }
    });
  }
}

module.exports = {
  createWebSocketServer,
  notifyProductInfosChange,
};
