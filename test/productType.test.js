// const expect = require("chai").expect;
// const request = require("supertest");
// const ProductType = require("../models/productType.model");
// const app = require("../app");
// const mongoose = require('mongoose');
// const config = require('../config');
// const env = process.env.NODE_ENV || 'development';

// describe("/productTypes", () => {
//   before(async () => {
//     await ProductType.deleteMany({});
//   });

//   after(async () => {
//     await mongoose.disconnect();
//   });

//   it("should connect and disconnect to mongodb", async () => {
//       // console.log(mongoose.connection.states);
//       mongoose.disconnect();
//       mongoose.connection.on('disconnected', () => {
//         expect(mongoose.connection.readyState).to.equal(0);
//       });
//       mongoose.connection.on('connected', () => {
//         expect(mongoose.connection.readyState).to.equal(1);
//       });
//       mongoose.connection.on('error', () => {
//         expect(mongoose.connection.readyState).to.equal(99);
//       });

//       await mongoose.connect(config.db[env], config.dbParams);
//   });

//   describe("GET /", () => {
//     it("should return all product type", async () => {
//       const productTypes = [
//         { name: "Chăm sóc da", description: "Dòng sản phẩm chăm sóc da"},
//         { name: "Chăm sóc tóc", description: "Dòng sản phẩm chăm sóc tóc"}
//       ];
//       // console.log(drinks);
//       await ProductType.insertMany(productTypes);
//       const res = await request(app).get("/productTypes");
//       console.log(res.body.data.length);
//       expect(res.status).to.equal(200);
//       expect(res.body.data.length).to.equal(2);
//     });
//   });

//   describe("GET/:id", () => {
//     it("should return a product type if valid id is passed", async () => {
//       const productType = { 
//         _id: new mongoose.Types.ObjectId, 
//         name: "Body care",
//         description: "chăm sóc cơ thể"
//       };
//       await ProductType.create(productType);
//       //thành công
//       const res = await request(app).get("/productTypes/" + productType._id);
//       // console.log(res);
//       expect(res.status).to.equal(200);
//       expect(res.body.data).to.have.property("name", productType.name);
//       expect(res.body.data).to.have.property("description", productType.description);
//     });
//     //id truyền vào ko hợp lệ
//     it("should return 400 error when invalid object id is passed", async () => {
//       const res = await request(app).get("/productTypes/1");
//       expect(res.status).to.equal(400);
//     });
//     //id truyền vào hợp lệ nhưng id này ko tìm thấy bản ghi tương ứng
//     it("should return 404 error when valid object id is passed but does not exist", async () => {
//       const res = await request(app).get("/productTypes/5f43ef20c1d4a133e4628181");
//       expect(res.status).to.equal(404);
//     });
//   });

//   describe("POST /", () => {
//     it("should return productType when the all request body is valid", async () => {
//       const res = await request(app)
//         .post("/productTypes")
//         .send({ 
//           _id: new mongoose.Types.ObjectId, 
//           reqName: "Pharmacy",
//           reqDescription: "Thuốc uống"
//         });
//       const data = res.body.data;
//       console.log(data);
//       expect(res.status).to.equal(201);
//       expect(data).to.have.property("_id");
//       expect(data).to.have.property("name", "Pharmacy");
//       expect(data).to.have.property("description", "Thuốc uống");
//       const productType = await ProductType.findOne({ name: 'Pharmacy' });
//       expect(productType.name).to.equal('Pharmacy');
//       expect(productType.description).to.equal('Thuốc uống');
//     });
//   });

//   describe("PUT /:id", () => {
//     it("should update the existing productType and return 200", async() => {
//       const productType = { 
//         _id: new mongoose.Types.ObjectId, 
//         name: "Clothes",
//         description: "Thời trang"
//       };
//       await ProductType.create(productType);
//         const res = await request(app)
//             .put("/productTypes/" + productType._id)
//             .send({
//               reqName: "Fashion",
//           });
//       const data = await res.body.data;
//       console.log(res.body);
//       expect(res.status).to.equal(200);
//       expect(data).to.have.property("name", "Fashion");
//       expect(data).to.have.property("description", "Thời trang");
//     });
//   });

//   describe("DELETE /:id", () => {
//     it("should delete requested id and return response 200", async () => {
//       const productType = new ProductType({
//             _id: new mongoose.Types.ObjectId,
//             name: "Drink",
//             description: "đồ uống"
//           });
//           await productType.save();
//           productTypeId = productType._id;
//       const res = await request(app).delete("/productTypes/" + productTypeId);
//       expect(res.status).to.be.equal(200); //status 200 nếu xóa ok
//     });
//     //sau khi xóa thì kiểm tra getbyid xem còn sp đó ko, nếu trả về 404 là ok
//     it("should return 404 when deleted drinks is requested", async () => {
//       let res = await request(app).get("/productTypes/" + productTypeId);
//       expect(res.status).to.be.equal(404);
//     });
//   });
// });