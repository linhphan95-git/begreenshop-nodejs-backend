// const expect = require("chai").expect;
// const request = require("supertest");
// const OrderDetail  = require("../models/orderDetail.model");
// const Order  = require("../models/order.model");
// const app = require("../app");
// const mongoose = require('mongoose');
// const config = require('../config');
// const env = process.env.NODE_ENV || 'development';

// describe("/orderDetails", () => {
//   before(async () => {
//     await OrderDetail.deleteMany({});
//   });

//   after(async () => {
//     mongoose.disconnect();
//   });

//   it("should connect and disconnect to mongodb", async () => {
//       // console.log(mongoose.connection.states);
//       mongoose.disconnect();
//       mongoose.connection.on('disconnected', () => {
//         expect(mongoose.connection.readyState).to.equal(0);
//       });
//       mongoose.connection.on('connected', () => {
//         expect(mongoose.connection.readyState).to.equal(1);
//       });
//       mongoose.connection.on('error', () => {
//         expect(mongoose.connection.readyState).to.equal(99);
//       });

//       await mongoose.connect(config.db[env], config.dbParams);
//   });

//   describe("GET /", () => {
//     it("should return all orders", async () => {
//       const orderDetails = [
//         { 
//           product: "660633b6a910091a6c362e27",
//           quantity: 2
//         },
//         { 
//           product: "660633b6a910091a6c362e28",
//           quantity: 3
//         }
//       ];
//       await OrderDetail.insertMany(orderDetails);
//       const res = await request(app).get("/orderDetails");
//       expect(res.status).to.equal(200);
//       expect(res.body.data.length).to.equal(2);
//     });
//   });

//   describe("GET/:id", () => {
//     it("should return a order if valid id is passed", async () => {
//       const orderDetail = new OrderDetail({
//         _id: new mongoose.Types.ObjectId,
//         product: "660633b6a910091a6c362e27",
//         quantity: 2
//       });
//       await orderDetail.save();
//       const res = await request(app).get("/orderDetails/" + orderDetail._id);
//       expect(res.status).to.equal(200);
//       expect(res.body.data).to.have.property("quantity", orderDetail.quantity);
//     });

//     it("should return 400 error when invalid object id is passed", async () => {
//       const res = await request(app).get("/orderDetails/1");
//       expect(res.status).to.equal(400);
//     });

//     it("should return 404 error when valid object id is passed but does not exist", async () => {
//       const res = await request(app).get("/orderDetails/5f43ef20c1d4a133e4628181");
//       expect(res.status).to.equal(404);
//     });
//   });

//   describe("POST /", () => {
//     it("should return order when the all request body is valid", async () => {
//       const res = await request(app)
//         .post("/orderDetails")
//         .send({
//           reqProduct: "660633b6a910091a6c362e27",
//           reqQuantity: 5,
//           orderid: "6607dcee89df3145dc172016"
//         });
//         // console.log(user_id);
//       const data = res.body.data;
//       expect(res.status).to.equal(201);
//       expect(data).to.have.property("_id");
//       expect(data).to.have.property("quantity", 5);
//       expect(data).to.have.property("product", "660633b6a910091a6c362e27");
//       const orderDetailsOfOrder = await Order.findOne({ orderDetails: data._id }).populate('orderDetails');
//       console.log(orderDetailsOfOrder);
//       expect(orderDetailsOfOrder.orderDetails[0].quantity).to.equal(5);
//     });
//   });

//   describe("PUT /:id", () => {
//     it("should update the existing orderDetail and return 200", async() => {
//       const orderDetail = new OrderDetail({
//         _id: new mongoose.Types.ObjectId,
//         product: "660633b6a910091a6c362e2e",
//         quantity: 5
//       });
//       await orderDetail.save();
//       const res = await request(app)
//           .put("/orderDetails/" + orderDetail._id)
//           .send({
//             reqQuantity: 10
//           });

//       expect(res.status).to.equal(200);
//       expect(res.body.data).to.have.property("quantity", 10);
//       expect(res.body.data).to.have.property("product", "660633b6a910091a6c362e2e");
//     });
//   });

//   describe("DELETE /:id", () => {
//     it("should delete requested id and return response 200", async () => {
//       const orderDetail = new OrderDetail({
//         _id: new mongoose.Types.ObjectId,
//         product: "660633b6a910091a6c362e2e",
//         quantity: 5
//       });
//       await orderDetail.save();
//       orderDetailId = orderDetail._id;
//       const res = await request(app).delete("/orderDetails/" + orderDetailId);
//       expect(res.status).to.be.equal(200);
//     });

//     it("should return 404 when deleted user is requested", async () => {
//       let res = await request(app).get("/orderDetails/" + orderDetailId);
//       expect(res.status).to.be.equal(404);
//     });
//   });
// });