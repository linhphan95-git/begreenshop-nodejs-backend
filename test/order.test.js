// const expect = require("chai").expect;
// const request = require("supertest");
// const Order  = require("../models/order.model");
// const Customer  = require("../models/customer.model");
// const app = require("../app");
// const mongoose = require('mongoose');
// const config = require('../config');
// const env = process.env.NODE_ENV || 'development';

// describe("/orders", () => {
//   before(async () => {
//     await Order.deleteMany({});
//   });

//   after(async () => {
//     mongoose.disconnect();
//   });

//   it("should connect and disconnect to mongodb", async () => {
//       // console.log(mongoose.connection.states);
//       mongoose.disconnect();
//       mongoose.connection.on('disconnected', () => {
//         expect(mongoose.connection.readyState).to.equal(0);
//       });
//       mongoose.connection.on('connected', () => {
//         expect(mongoose.connection.readyState).to.equal(1);
//       });
//       mongoose.connection.on('error', () => {
//         expect(mongoose.connection.readyState).to.equal(99);
//       });

//       await mongoose.connect(config.db[env], config.dbParams);
//   });

//   describe("GET /", () => {
//     it("should return all orders", async () => {
//       const orders = [
//         { 
//           shippedDate: "2024-03-30T11:40:46.855+00:00",
//           note: "Đơn hàng mới",
//           cost: 200000
//         },
//         { 
//           shippedDate: "2024-04-30T11:40:46.855+00:00",
//           note: "Đơn hàng mới",
//           cost: 300000
//         }
//       ];
//       await Order.insertMany(orders);
//       const res = await request(app).get("/orders");
      
//       expect(res.status).to.equal(200);
//       expect(res.body.data.length).to.equal(2);
//     });
//   });

//   describe("GET/:id", () => {
//     it("should return a order if valid id is passed", async () => {
//       const order = new Order({
//         _id: new mongoose.Types.ObjectId,
//         shippedDate: "2024-03-30T11:40:46.855+00:00",
//         note: "Đơn hàng mới",
//         cost: 300000
//       });
//       await order.save();
//       const res = await request(app).get("/orders/" + order._id);
//       expect(res.status).to.equal(200);
//       expect(res.body.data).to.have.property("cost", order.cost);
//     });

//     it("should return 400 error when invalid object id is passed", async () => {
//       const res = await request(app).get("/orders/1");
//       expect(res.status).to.equal(400);
//     });

//     it("should return 404 error when valid object id is passed but does not exist", async () => {
//       const res = await request(app).get("/orders/5f43ef20c1d4a133e4628181");
//       expect(res.status).to.equal(404);
//     });
//   });

//   describe("POST /", () => {
//     it("should return order when the all request body is valid", async () => {
//       const res = await request(app)
//         .post("/orders")
//         .send({
//           reqShippedDate:"2024-03-26T11:40:46.855+00:00",
//           reqNote : "no problem",
//           reqCost: 50000, 
//           customerid: "66063aacfc46324608f63cb1"
//         });
//         // console.log(user_id);
//       const data = res.body.data;
//       expect(res.status).to.equal(201);
//       expect(data).to.have.property("_id");
//       expect(data).to.have.property("cost", 50000);
//       const orderOfCustomer = await Customer.findOne({ orders: data._id }).populate('orders');
//       expect(orderOfCustomer.orders[0].cost).to.equal(50000);
//       expect(orderOfCustomer.orders[0].note).to.equal('no problem');
//     });
//   });

//   describe("PUT /:id", () => {
//     it("should update the existing order and return 200", async() => {
//       const order = new Order({
//         _id: new mongoose.Types.ObjectId,
//         shippedDate: "2024-03-30T11:40:46.855+00:00",
//         note: "Đơn hàng mới",
//         cost: 300000
//       });
//       await order.save();
//       const res = await request(app)
//           .put("/orders/" + order._id)
//           .send({
//             reqNote: "Đơn hàng đặc biệt",
//             reqCost: 700000,
//           });

//       expect(res.status).to.equal(200);
//       expect(res.body.data).to.have.property("note", "Đơn hàng đặc biệt");
//       expect(res.body.data).to.have.property("cost", 700000);
//     });
//   });

//   describe("DELETE /:id", () => {
//     it("should delete requested id and return response 200", async () => {
//       const order = new Order({
//         _id: new mongoose.Types.ObjectId,
//         shippedDate: "2024-03-30T11:40:46.855+00:00",
//         note: "Đơn hàng mới",
//         cost: 300000
//       });
//       await order.save();
//       orderId = order._id;
//       const res = await request(app).delete("/orders/" + orderId);
//       expect(res.status).to.be.equal(200);
//     });

//     it("should return 404 when deleted user is requested", async () => {
//       let res = await request(app).get("/orders/" + orderId);
//       expect(res.status).to.be.equal(404);
//     });
//   });
// });