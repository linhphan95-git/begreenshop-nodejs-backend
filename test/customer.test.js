// const expect = require("chai").expect;
// const request = require("supertest");
// const Customer  = require("../models/customer.model");
// const app = require("../app");
// const mongoose = require('mongoose');
// const config = require('../config');
// const env = process.env.NODE_ENV || 'development';

// describe("/customers", () => {
//   before(async () => {
//     await Customer.deleteMany({});
//   });

//   after(async () => {
//     mongoose.disconnect();
//   });

//   it("should connect and disconnect to mongodb", async () => {
//       // console.log(mongoose.connection.states);
//       mongoose.disconnect();
//       mongoose.connection.on('disconnected', () => {
//         expect(mongoose.connection.readyState).to.equal(0);
//       });
//       mongoose.connection.on('connected', () => {
//         expect(mongoose.connection.readyState).to.equal(1);
//       });
//       mongoose.connection.on('error', () => {
//         expect(mongoose.connection.readyState).to.equal(99);
//       });

//       await mongoose.connect(config.db[env], config.dbParams);
//   });

//   describe("GET /", () => {
//     it("should return all customers", async () => {
//       const customers = [
//         { 
//           fullName: "george",
//           email: "geo@gmail.com", 
//           address: "1 Notre-dame", 
//           phone: "0905057999",
//           city: "Paris",
//           country: "French"
//         },
//         { 
//           fullName: "Alex",
//           email: "Alex@gmail.com", 
//           address: "1 Notre-dame", 
//           phone: "0905057998",
//           city: "Paris",
//           country: "French"
//         }
//       ];
//       await Customer.insertMany(customers);
//       const res = await request(app).get("/customers");
      
//       expect(res.status).to.equal(200);
//       expect(res.body.data.length).to.equal(2);
//     });
//   });

//   describe("GET/:id", () => {
//     it("should return a user if valid id is passed", async () => {
//       const customer = new Customer({ 
//         _id: new mongoose.Types.ObjectId,
//         fullName: "florent",
//         email: "florent@gmail.com", 
//         address: "1 Notre-dame", 
//         phone: "0905057996",
//         city: "Paris",
//         country: "French"
//       });
//       await customer.save();
//       const res = await request(app).get("/customers/" + customer._id);
//       expect(res.status).to.equal(200);
//       expect(res.body.data).to.have.property("fullName", customer.fullName);
//     });

//     it("should return 400 error when invalid object id is passed", async () => {
//       const res = await request(app).get("/customers/1");
//       expect(res.status).to.equal(400);
//     });

//     it("should return 404 error when valid object id is passed but does not exist", async () => {
//       const res = await request(app).get("/customers/5f43ef20c1d4a133e4628181");
//       expect(res.status).to.equal(404);
//     });
//   });

//   describe("POST /", () => {
//     it("should return customer when the all request body is valid", async () => {
//       const res = await request(app)
//         .post("/customers")
//         .send({
//           reqFullName: "esteve",
//           reqEmail: "esteve@gmail.com",
//           reqAddress: "spain",
//           reqPhone: "0905057995",
//           reqCity:"barcelona",
//           reqCountry:"spain"
//         });
//       const data = res.body.data;
//       expect(res.status).to.equal(201);
//       expect(data).to.have.property("_id");
//       expect(data).to.have.property("fullName", "esteve");
//       expect(data).to.have.property("email", "esteve@gmail.com");
//       expect(data).to.have.property("address", "spain");
//       expect(data).to.have.property("phone", "0905057995");
//       expect(data).to.have.property("city", "barcelona");
//       expect(data).to.have.property("country", "spain");

//       const customer = await Customer.findOne({ email: 'esteve@gmail.com' });
//       expect(customer.fullName).to.equal('esteve');
//       expect(customer.email).to.equal('esteve@gmail.com');
//     });
//   });

//   describe("PUT /:id", () => {
//     it("should update the existing customer and return 200", async() => {
//       const customer = new Customer({ 
//         _id: new mongoose.Types.ObjectId,
//         fullName: "florence",
//         email: "florence@gmail.com", 
//         address: "1 Notre-dame", 
//         phone: "0905057994",
//         city: "Paris",
//         country: "French"
//       });
//       await customer.save();
//         const res = await request(app)
//             .put("/customers/" + customer._id)
//             .send({
//               reqFullName: "juan",
//               reqEmail: "juan@gmail.com",
//             });

//       expect(res.status).to.equal(200);
//       expect(res.body.data).to.have.property("fullName", "juan");
//       expect(res.body.data).to.have.property("email", "juan@gmail.com");
//     });
//   });

//   describe("DELETE /:id", () => {
//     it("should delete requested id and return response 200", async () => {
//       const customer = new Customer({ 
//         _id: new mongoose.Types.ObjectId,
//         fullName: "elons Musk",
//         email: "elons@gmail.com", 
//         address: "1 Notre-dame", 
//         phone: "0905057993",
//         city: "New york",
//         country: "USA"
//       });
//       await customer.save();
//       customerId = customer._id;
//       const res = await request(app).delete("/customers/" + customerId);
//       expect(res.status).to.be.equal(200);
//     });

//     it("should return 404 when deleted customer is requested", async () => {
//       let res = await request(app).get("/customers/" + customerId);
//       expect(res.status).to.be.equal(404);
//     });
//   });
// });