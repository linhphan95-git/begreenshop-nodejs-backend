// const expect = require("chai").expect;
// const request = require("supertest");
// const Product  = require("../models/product.model");
// const ProductType  = require("../models/productType.model");
// const app = require("../app");
// const mongoose = require('mongoose');
// const config = require('../config');
// const env = process.env.NODE_ENV || 'development';

// describe("/products", () => {
//   before(async () => {
//     await Product.deleteMany({});
//   });

//   after(async () => {
//     mongoose.disconnect();
//   });

//   it("should connect and disconnect to mongodb", async () => {
//       // console.log(mongoose.connection.states);
//       mongoose.disconnect();
//       mongoose.connection.on('disconnected', () => {
//         expect(mongoose.connection.readyState).to.equal(0);
//       });
//       mongoose.connection.on('connected', () => {
//         expect(mongoose.connection.readyState).to.equal(1);
//       });
//       mongoose.connection.on('error', () => {
//         expect(mongoose.connection.readyState).to.equal(99);
//       });

//       await mongoose.connect(config.db[env], config.dbParams);
//   });

//   describe("GET /", () => {
//     it("should return all products", async () => {
//       const products = [
//         { name: "Sunsilk",
//           description: "Dầu gội",
//           type: "66062a9baf09be0ef8921917",
//           imageUrl: "hinhanh.img",
//           buyPrice: 200000,
//           promotionPrice: 150000,
//           amount: 5
//         },
//         { name: "LifeBoy",
//           description: "Dầu gội",
//           type: "66062a9baf09be0ef8921917",
//           imageUrl: "hinhanh.img",
//           buyPrice: 200000,
//           promotionPrice: 150000,
//           amount: 5
//         }
//       ];
//       await Product.insertMany(products);
//       const res = await request(app).get("/products");

//       expect(res.status).to.equal(200);
//       expect(res.body.data.length).to.equal(2);
//     });
//   });

//   describe("GET/:id", () => {
//     it("should return a item if valid id is passed", async () => {
//       const product = new Product({
//         _id: new mongoose.Types.ObjectId,
//         name: "Rejoyce",
//         description: "Dầu gội",
//         type: "66062a9baf09be0ef8921917",
//         imageUrl: "hinhanh.img",
//         buyPrice: 200000,
//         promotionPrice: 150000,
//         amount: 5
//       });
//       await product.save();
//       const res = await request(app).get("/products/" + product._id);
//       expect(res.status).to.equal(200);
//       expect(res.body.data).to.have.property("name", product.name);
//     });
//     it("should return 400 error when invalid object id is passed", async () => {
//       const res = await request(app).get("/products/1");
//       expect(res.status).to.equal(400);
//     });

//     it("should return 404 error when valid object id is passed but does not exist", async () => {
//       const res = await request(app).get("/products/5f43ef20c1d4a133e4628181");
//       expect(res.status).to.equal(404);
//     });
//   });

//   describe("POST /", () => {
//     it("should return product when the all request body is valid", async () => {
//       const res = await request(app)
//         .post("/products")
//         .send({
//           reqName: "pond",
//           reqDescription: "Sữa rửa mặt",
//           reqType: "66062a9baf09be0ef8921916",
//           reqImageUrl:"anh.png",
//           reqBuyPrice: 200000,
//           reqPromotionPrice:100000,
//           reqAmount: 50
//         });
//         // console.log(user_id);
//       const data = res.body.data;
//       expect(res.status).to.equal(201);
//       expect(data).to.have.property("_id");
//       expect(data).to.have.property("name", "pond");
//       expect(data).to.have.property("description", "Sữa rửa mặt");
//       expect(data).to.have.property("type", "66062a9baf09be0ef8921916");
//       expect(data).to.have.property("buyPrice", 200000);
//       expect(data).to.have.property("promotionPrice", 100000);
//       expect(data).to.have.property("amount", 50);
//     });
//   });

//   describe("PUT /:id", () => {
//     it("should update the existing product and return 200", async() => {
//         const product = new Product({
//           _id: new mongoose.Types.ObjectId,
//           name: "Dove",
//           description: "Dầu gội",
//           type: "66062a9baf09be0ef8921917",
//           imageUrl: "hinhanh.img",
//           buyPrice: 200000,
//           promotionPrice: 150000,
//           amount: 5
//         });
//         await product.save();
//         const res = await request(app)
//             .put("/products/" + product._id)
//             .send({
//               reqDescription: "Sữa tắm",
//               reqType: "66062a9baf09be0ef892191a"
//             });
//               console.log(res.body);
//       expect(res.status).to.equal(200);
//       expect(res.body.data).to.have.property("description", "Sữa tắm");
//       expect(res.body.data).to.have.property("type", "66062a9baf09be0ef892191a");
//     });
//   });

//   describe("DELETE /:id", () => {
//     it("should delete requested id and return response 200", async () => {
//       const product = new Product({
//         _id: new mongoose.Types.ObjectId,
//         name: "Dove Dove",
//         description: "Dầu gội",
//         type: "66062a9baf09be0ef8921917",
//         imageUrl: "hinhanh.img",
//         buyPrice: 200000,
//         promotionPrice: 150000,
//         amount: 5
//       });
//       await product.save();
//       productId = product._id;
//       const res = await request(app).delete("/products/" + productId);
//       expect(res.status).to.be.equal(200);
//     });

//     it("should return 404 when deleted product is requested", async () => {
//       let res = await request(app).get("/products/" + productId);
//       expect(res.status).to.be.equal(404);
//     });
//   });
// });
