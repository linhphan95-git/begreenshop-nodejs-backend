const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = new Schema({  
  _id: Schema.Types.ObjectId,
	orderDate: {
    type: Date, 
    default: Date.now(),
  },
	shippedDate:{
    type: Date,
    default: () => new Date(Date.now() + 7 * 24 * 60 * 60 * 1000) // 1 tuần sau ngày hiện tại
  },
  note:{
    type: String
  },
  //1 order có nhiều orders detail
	orderDetails: [{
   type: mongoose.Types.ObjectId, 
   ref: "OrderDetails"
  }],
	cost: {
    type: Number,
    default: 0
  },
  shipping: {
    type: Number,
    default: 5
  },
	status: {
    type: String,
    default: "ordered"
  },
  //1 order thuộc 1 customer 
  customer: {
    type: mongoose.Types.ObjectId,
    ref: "Customer",
    required: true
  },
	addressDelivery: {
    type: String
  },
  deletedAt: {
    type: Date,
    default: null
  }
  }, {
    timestamps: true
  });
module.exports = mongoose.model("Order", orderSchema);