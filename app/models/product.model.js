const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema({
  _id: Schema.Types.ObjectId,
	name: {
    type: String, 
    unique: true,
    required: true
  },
	buyPrice: {
    type: Number, 
    required: true
  },
	promotionPrice: {
    type: Number,
    required: true
  },
	description:{
    type: String
  },
  amount: {
    type: Number,
    default: 0
  }, 
  weight: {
    type: Number, 
    required: false
  },
	imageUrl: {
    type: Array,
    required: true
  },
  origin: {
    type: String, 
    required: false
  },
  //1 sp chỉ thuộc 1 type
	type: {
   type: mongoose.Types.ObjectId, 
   ref: "product_types", 
   required: true
  },
  deletedAt: {
    type: Date, default: null
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
}, {
  timestamps: true
})
productSchema.methods.softDelete = function() {
  this.deletedAt = new Date();
  return this.save();
};

productSchema.methods.restore = function() {
  this.deletedAt = null;
  return this.save();
};

module.exports = mongoose.model("products", productSchema);