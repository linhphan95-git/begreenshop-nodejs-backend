const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");

const userModel = new mongoose.Schema({
    // _id: Schema.Types.ObjectId,
    username: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        required: true
    }, 
    password: {
        type: String,
        required: true
    },
    avatar: {
        type: String,
        required: false
    },
    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Customer"
    },
    roles: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role"
    }],
    //1 user tương ứng 1 userDetail
      userDetail: {
      type: mongoose.Types.ObjectId,
      ref: "UserDetail", 
    },
    deletedAt: {
        type: Date,
        default: null
    },
    resetPasswordToken: String,
    resetPasswordExpires: Date,
}, {
    timestamps: true
})
// userModel.methods.softDelete = function() {
//     this.deletedAt = new Date();
//     return this.save();
// };
// userModel.js
userModel.methods.softDelete = async function() {
    const now = new Date();
    this.deletedAt = now;
    await this.save();
  
    // Find the associated customer and update the deletedAt timestamp
    const customer = await mongoose.model("Customer").findById(this.customer);
    if (customer) {
      customer.deletedAt = now;
      await customer.save();
  
      // Find the associated orders and update the deletedAt timestamp
      const orders = await mongoose.model("Order").find({ _id: { $in: customer.orders } });
      for (const order of orders) {
        order.deletedAt = now;
        await order.save();
  
        // Find the associated orderDetails and update the deletedAt timestamp
        const orderDetails = await mongoose.model("OrderDetail").find({ order: order._id });
        for (const orderDetail of orderDetails) {
          orderDetail.deletedAt = now;
          await orderDetail.save();
        }
      }
    }
  };
// userModel.methods.restore = function() {
//     this.deletedAt = null;
//     return this.save();
// };
// userModel.js
userModel.methods.restore = async function() {
    this.deletedAt = null;
    await this.save();
  
    // Tìm kiếm đối tượng Customer liên quan đến người dùng
    const customer = await mongoose.model("Customer").findById(this.customer);
    if (customer) {
      customer.deletedAt = null;
      await customer.save();
  
      // Tìm kiếm các đối tượng Order liên quan đến Customer và cập nhật trường deletedAt
      const orders = await mongoose.model("Order").find({ _id: { $in: customer.orders } });
      for (const order of orders) {
        order.deletedAt = null;
        await order.save();
  
        // Tìm kiếm các đối tượng OrderDetail liên quan đến Order và cập nhật trường deletedAt
        const orderDetails = await mongoose.model("OrderDetail").find({ order: order._id });
        for (const orderDetail of orderDetails) {
          orderDetail.deletedAt = null;
          await orderDetail.save();
        }
      }
    }
  };
    
module.exports = mongoose.model("User", userModel);