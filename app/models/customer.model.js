const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const customerSchema = new Schema({
  _id: Schema.Types.ObjectId,
	fullName: {
    type: String, 
    required: true
  },
	phone:{
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  //1 customer có nhiều orders
	orders: [{
   type: mongoose.Types.ObjectId, 
   ref: "Order", 
  }],
	address: {
    type: String,
    default: ""
  },
  state: {
    type: String,
    default: ""
  },
  city: {
    type: String,
    default: ""
  },
  country: {
    type: String,
    default: ""
  },
  deletedAt: {
    type: Date,
    default: null
  }
}, {
  timestamps: true
});
module.exports = mongoose.model("Customer", customerSchema);