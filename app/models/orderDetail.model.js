const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderDetailSchema = new Schema({
  _id: Schema.Types.ObjectId,
	quantity: {
    type: Number, 
    default: 0
  },
	product: {
   type: mongoose.Types.ObjectId, 
   ref: "products", 
  },
  deletedAt: {
    type: Date,
    default: null
  }
  }, {
    timestamps: true
  });
module.exports = mongoose.model("OrderDetails", orderDetailSchema);