const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productTypeSchema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      require: true,
    },
    description: {
      type: String,
    },
    imageUrl: {
      type: String,
      require: true,
    },
    imageUrlBackground: {
      type: String,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("product_types", productTypeSchema);
