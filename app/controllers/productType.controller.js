const { json } = require("body-parser");
const productTypeModel = require("../models/productType.model");
const mongoose = require("mongoose");
//lấy tất cả type product
const getAllProductType = async (req, res) => {
  const condition = {};
  const searchContent = req.query.searchContent;
  const limit = req.query.limit;
  const page = req.query.page;
  if (searchContent) {
    condition.name = { $regex: new RegExp(searchContent, "i") };
  }
  condition.deletedAt = null;
  try {
    const result = await productTypeModel
      .find(condition)
      // .sort({ createdAt: -1 })
      .limit(limit)
      .skip((page - 1) * limit);
    return res.status(200).json({
      message: "Lấy tất cả dữ liệu thành công",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "đã có lỗi xảy ra",
    });
  }
};
//tạo 1 type mới
const createProductType = async (req, res) => {
  const { reqName, reqImageUrl, reqImageUrlBackground, reqDescription } =
    req.body;
  if (!reqName) {
    return res.status(400).json({
      message: "product name is required",
    });
  }
  try {
    const newItem = {
      name: reqName,
      description: reqDescription,
      imageUrl: reqImageUrl,
      imageUrlBackground: reqImageUrlBackground,
    };
    const result = await productTypeModel.create(newItem);
    return res.status(201).json({
      message: "Create successfully",
      data: result,
    });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(500).json({
        message: "Name is duplicate",
      });
    } else {
      return res.status(500).json({
        message: "Have an error",
      });
    }
  }
};
//lấy type theo id
const getProductTypeByID = async (req, res) => {
  const typeId = req.params.typeId;
  if (!mongoose.Types.ObjectId.isValid(typeId)) {
    return res.status(400).json({
      message: "ID nhập vào ko hợp lệ",
    });
  }
  try {
    const result = await productTypeModel.findById(typeId);
    if (result) {
      return res.status(200).json({
        message: "Lấy thông tin theo id thành công",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "Id ko hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra",
    });
  }
};
//chỉnh sửa thông tin type
const updateProductType = async (req, res) => {
  const typeId = req.params.typeId;
  if (!mongoose.Types.ObjectId.isValid(typeId)) {
    return res.status(400).json({
      message: "ID invalid",
    });
  }
  const { reqName, reqDescription, reqImageUrl, reqImageUrlBackground } =
    req.body;
  if (reqName === "") {
    return res.status(400).json({
      message: "name is empty",
    });
  }
  try {
    const newItemUpdate = {};
    if (reqName) {
      newItemUpdate.name = reqName;
    }
    if (reqImageUrl) {
      newItemUpdate.imageUrl = reqImageUrl;
    }
    if (reqDescription) {
      newItemUpdate.description = reqDescription;
    }
    if (reqImageUrlBackground) {
      newItemUpdate.imageUrlBackground = reqImageUrlBackground;
    }
    const result = await productTypeModel.findByIdAndUpdate(
      typeId,
      newItemUpdate
    );
    if (result) {
      return res.status(200).json({
        message: "Update Successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID invalid",
      });
    }
  } catch (error) {
    if (error.code === 11000) {
      return res.status(500).json({
        message: "Name is duplicate",
      });
    } else {
      return res.status(500).json({
        message: "Have an error",
      });
    }
  }
};
//xóa type sp
const deleteProductType = async (req, res) => {
  const typeId = req.params.typeId;
  if (!mongoose.Types.ObjectId.isValid(typeId)) {
    res.status(400).json({
      message: "ID ko hợp lệ",
    });
  }
  try {
    const result = await productTypeModel.findByIdAndDelete(typeId);
    if (result) {
      return res.status(200).json({
        message: "Xóa thành công",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID ko hợp lệ",
      });
    }
  } catch (error) {
    res.json({
      message: "Có lỗi xảy ra",
    });
  }
};
//xóa mềm type sp
const softDeleteTypeProduct = async (req, res) => {
  const typeId = req.params.typeId;

  // Kiểm tra ID hợp lệ
  if (!mongoose.Types.ObjectId.isValid(typeId)) {
    return res.status(400).json({
      message: "ID invalid",
    });
  }
  try {
    // Tìm đơn hàng theo ID
    const type = await productTypeModel.findById(typeId);
    // Kiểm tra nếu đơn hàng không tồn tại
    if (!type) {
      return res.status(404).json({ message: "type not found" });
    }
    // Cập nhật deletedAt thành thời gian hiện tại để thực hiện soft delete
    type.deletedAt = new Date();
    await type.save();
    // Kiểm tra kết quả cập nhật
    res.status(200).json({
      message: "Type Deleted Successfully",
      data: type,
    });
  } catch (error) {
    res.status(500).json({
      message: "Have an error",
    });
  }
};
//lấy tất cả type xóa mềm
const getAllTypesDeleted = async (req, res) => {
  try {
    //get type in softDelete (deletedAt !== null)
    const result = await productTypeModel.find({ deletedAt: { $ne: null } });
    return res.status(200).json({
      message: "Get all types deleted successfully",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Have an error",
    });
  }
};
const restoreSoftDeleteType = async (req, res) => {
  const typeId = req.params.typeId;

  // Kiểm tra ID hợp lệ
  if (!mongoose.Types.ObjectId.isValid(typeId)) {
    return res.status(400).json({
      message: "ID invalid",
    });
  }
  try {
    const typeRestored = await productTypeModel.findById(typeId);
    if (!typeRestored) {
      return res.status(404).json({ message: "type not found" });
    }

    typeRestored.deletedAt = null;
    await typeRestored.save();

    res.status(200).json({
      message: "Type restored successfully",
      data: typeRestored,
    });
  } catch (error) {
    res.status(500).json({ message: "Have an error" });
  }
};
module.exports = {
  getAllProductType,
  createProductType,
  getProductTypeByID,
  updateProductType,
  deleteProductType,
  softDeleteTypeProduct,
  getAllTypesDeleted,
  restoreSoftDeleteType,
};
