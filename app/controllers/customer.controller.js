const mongoose = require("mongoose");
const customerModel = require("../models/customer.model");
const orderModel = require("../models/order.model");
const XLSX = require("xlsx");
const fs = require("fs");

//hàm xuất thông tin bảng ra file excel
const exportCustomersToExcel = async (req, res) => {
  try {
    // Loại trừ các trường password và role từ kết quả truy vấn
    const result = await customerModel.find();

    if (result && result.length > 0) {
      const filePath = "customers.xlsx";
      const wb = XLSX.utils.book_new();
      // Convert customers to JSON and map to include _id
      const jsonResult = result.map((customer) => {
        const obj = customer.toObject();
        obj._id = customer._id.toString(); // Add id field explicitly
        return obj;
      });
      const ws = XLSX.utils.json_to_sheet(jsonResult);
      XLSX.utils.book_append_sheet(wb, ws, "Customers");
      XLSX.writeFile(wb, filePath);

      res.download(filePath, (err) => {
        if (err) {
        }
        // Xóa file sau khi gửi
        fs.unlinkSync(filePath);
      });
    } else {
      res.status(404).json({ message: "No users found" });
    }
  } catch (error) {
    res.status(500).json({ message: "Failed to fetch data" });
  }
};
//lấy tất cả customer
const getAllCustomer = async (req, res) => {
  const condition = {};
  condition.deletedAt = null; //lấy những order chưa bị xóa soft delete
  const searchContent = req.query.searchContent;
  const limit = req.query.limit;
  const page = req.query.page;
  if (searchContent) {
    condition.$or = [];
    // Check if searchContent is a valid ObjectId
    if (mongoose.Types.ObjectId.isValid(searchContent)) {
      condition.$or.push({ _id: searchContent });
    }

    condition.$or.push(
      { fullName: { $regex: new RegExp(searchContent, "i") } },
      { phone: { $regex: new RegExp(searchContent, "i") } },
      { email: { $regex: new RegExp(searchContent, "i") } },
      { address: { $regex: new RegExp(searchContent, "i") } },
      { country: { $regex: new RegExp(searchContent, "i") } },
      { city: { $regex: new RegExp(searchContent, "i") } }
    );
  }
  try {
    const result = await customerModel
      .find(condition)
      .limit(limit)
      .skip((page - 1) * limit);
    return res.status(200).json({
      message: "Lấy tất cả sp thành công",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Đã xảy ra lỗi",
    });
  }
};
const createCustomer = async (req, res) => {
  const { reqFullName, reqPhone, reqEmail, reqAddress, reqCity, reqCountry } =
    req.body;
  if (!reqFullName) {
    return res.status(400).json({
      message: "Tên sp ko hợp lệ",
    });
  }
  if (!reqPhone) {
    return res.status(400).json({
      message: "Tên product type ko hợp lệ",
    });
  }
  try {
    const newItem = {
      _id: new mongoose.Types.ObjectId(),
      fullName: reqFullName,
      phone: reqPhone,
      email: reqEmail,
      address: reqAddress,
      city: reqCity,
      country: reqCountry,
    };
    const result = await customerModel.create(newItem);
    return res.status(201).json({
      message: " Tạo mới thành công",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "có lỗi xảy ra",
    });
  }
};
//lấy customer theo id
const getCustomerById = async (req, res) => {
  const customerid = req.params.customerid;
  if (!mongoose.Types.ObjectId.isValid(customerid)) {
    return res.status(400).json({
      message: "ID nhập vào ko hợp lệ",
    });
  }
  try {
    const result = await customerModel.findById(customerid);
    if (result) {
      return res.status(200).json({
        message: "Lấy thông tin theo id thành công",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "Id ko hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra",
    });
  }
};
//lấy customer theo email
const getCustomerByEmail = async (req, res) => {
  const email = req.params.email;
  try {
    const result = await customerModel.find({ email: email });
    if (result.length > 0) {
      return res.status(200).json({
        message: "get customer by email",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "no customer found",
        data: result,
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Have an error",
    });
  }
};
//cập nhật thông tin người dùng khi đặt hàng
const updateCustomer = async (req, res) => {
  const customerid = req.params.customerid;
  if (!mongoose.Types.ObjectId.isValid(customerid)) {
    return res.status(400).json({
      message: "ID truyền vào ko hợp lệ",
    });
  }
  const { reqFullName, reqPhone, reqEmail, reqAddress, reqCity, reqCountry } =
    req.body;

  if (reqFullName == "") {
    return res.status(400).json({
      message: "Tên sp ko hợp lệ",
    });
  }
  if (reqPhone == "") {
    return res.status(400).json({
      message: "Điện thoại ko hợp lệ",
    });
  }
  if (reqEmail == "") {
    return res.status(400).json({
      message: "Email ko hợp lệ",
    });
  }
  try {
    const newItemUpdate = {};
    if (reqFullName) {
      newItemUpdate.fullName = reqFullName;
    }
    if (reqPhone) {
      newItemUpdate.phone = reqPhone;
    }
    if (reqEmail) {
      newItemUpdate.email = reqEmail;
    }
    if (reqAddress) {
      newItemUpdate.address = reqAddress;
    }
    if (reqCity) {
      newItemUpdate.city = reqCity;
    }
    if (reqCountry) {
      newItemUpdate.country = reqCountry;
    }
    const result = await customerModel.findByIdAndUpdate(
      customerid,
      newItemUpdate
    );
    if (result) {
      return res.status(200).json({
        message: "Cập nhật thành công",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID chưa hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra",
    });
  }
};
//xóa 1 khách hàng
const deleteCustomer = async (req, res) => {
  const customerid = req.params.customerid;
  if (!mongoose.Types.ObjectId.isValid(customerid)) {
    res.status(400).json({
      message: "ID ko hợp lệ",
    });
  }
  try {
    // Tìm khách hàng theo ID
    const customer = await customerModel
      .findById(customerid)
      .populate("orders");
    // Xóa các đơn hàng của khách hàng
    await orderModel.deleteMany({ _id: { $in: customer.orders } });

    // Xóa khách hàng
    const result = await customerModel.findByIdAndDelete(customerid);
    if (result) {
      return res.status(200).json({
        message: "Customer Deleted Successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID ko hợp lệ",
      });
    }
  } catch (error) {
    res.json({
      message: "Have an error",
    });
  }
};
module.exports = {
  getAllCustomer,
  createCustomer,
  getCustomerById,
  updateCustomer,
  deleteCustomer,
  getCustomerByEmail,
  exportCustomersToExcel,
};
