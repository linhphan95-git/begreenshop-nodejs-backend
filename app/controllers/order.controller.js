const mongoose = require("mongoose");
const orderModel = require("../models/order.model");
const customerModel = require("../models/customer.model");
const userModel = require("../models/user.model");
// const XLSX = require('xlsx');
// const fs = require('fs');

const XlsxPopulate = require("xlsx-populate");
const fs = require("fs");
const { ObjectId } = require("mongoose").Types;

const exportOrdersToExcel = async (req, res) => {
  try {
    const orders = await orderModel.find().populate({
      path: "orderDetails",
      populate: {
        path: "product",
        model: "products", // Thay đổi tên model nếu cần thiết
      },
    });

    if (!orders || orders.length === 0) {
      return res.status(404).json({ message: "No orders found" });
    }

    // Tạo một mảng dữ liệu cho sheet Excel
    const data = [];
    orders.forEach((order) => {
      order.orderDetails.forEach((detail) => {
        data.push({
          orderId: order._id.toString(),
          orderCreatedAt: new Date(order.createdAt),
          orderUpdatedAt: new Date(order.updatedAt),
          orderNote: order.note,
          orderCost: order.cost,
          detailId: detail._id.toString(),
          detailQuantity: detail.quantity,
          productName: detail.product.name,
          productDescription: detail.product.description,
          productOrigin: detail.product.origin,
          productWeight: detail.product.weight,
          productCreatedAt: new Date(detail.product.createdAt),
          productUpdatedAt: new Date(detail.product.updatedAt),
        });
      });
    });

    // Tạo file Excel
    const filePath = "orders.xlsx";
    await XlsxPopulate.fromBlankAsync().then((workbook) => {
      const sheet = workbook.addSheet("Orders");

      // Tiêu đề các cột
      const headers = [
        "Order ID",
        "Order Created At",
        "Order Updated At",
        "Order Note",
        "Order Cost",
        "Detail ID",
        "Detail Quantity",
        "Product Name",
        "Product Description",
        "Product Origin",
        "Product Weight",
        "Product Created At",
        "Product Updated At",
      ];

      // Đặt giá trị cho tiêu đề cột
      headers.forEach((header, index) => {
        sheet.cell(1, index + 1).value(header);
      });

      // Đặt giá trị cho từng dòng dữ liệu
      data.forEach((item, rowIndex) => {
        const row = rowIndex + 2; // Bắt đầu từ hàng thứ hai (sau tiêu đề)
        Object.keys(item).forEach((key, colIndex) => {
          const cell = sheet.cell(row, colIndex + 1);
          if (item[key] instanceof Date) {
            // Đặt giá trị ngày tháng và định dạng
            cell.value(item[key]).style("numberFormat", "dd/mm/yyyy hh:mm:ss");
          } else {
            cell.value(item[key]);
          }
        });
      });

      return workbook.toFileAsync(filePath);
    });

    // Gửi file Excel cho client và xóa file sau khi tải xuống
    res.download(filePath, (err) => {
      if (err) {
        res.status(500).json({ message: "Failed to download file" });
      }
      fs.unlinkSync(filePath); // Xóa file sau khi tải thành công
    });
  } catch (error) {
    res.status(500).json({ message: "Failed to fetch data" });
  }
};
//lấy tất cả order
const getAllOrder = async (req, res) => {
  const customerid = req.query.customerid;
  if (customerid && !mongoose.Types.ObjectId.isValid(customerid)) {
    res.status(400).json({
      message: "Id ko hợp lệ",
    });
  }
  var limit = req.query.limit;
  if (limit == "null") {
    limit = null;
  }
  var page = req.query.page;
  const searchContent = req.query.search;
  var shippedDate = req.query.shippedDate || "null";
  var orderDate = req.query.orderDate || "null";
  var ordered = req.query.ordered;
  var preparing = req.query.preparing;
  var received = req.query.received;
  var canceled = req.query.canceled;
  var delivered = req.query.delivered;

  const formatDateAndTime = (dateAndTime) => {
    // Chuyển đổi chuỗi thành số
    const timestamp = parseInt(dateAndTime, 10);
    // Tạo đối tượng Date từ timestamp
    const date = new Date(timestamp);
    // Chuyển đổi thành chuỗi ISO 8601
    const isoDateString = date.toISOString();
    return isoDateString;
  };

  try {
    const condition = {};
    if (customerid) {
      condition.customer = customerid;
    }

    condition.deletedAt = null; // lấy những order chưa bị xóa soft delete
    let roles = [];
    if (ordered === "true") {
      roles.push("ordered");
    }
    if (preparing === "true") {
      roles.push("preparing");
    }
    if (delivered === "true") {
      roles.push("delivering");
    }
    if (received === "true") {
      roles.push("received");
    }
    if (canceled === "true") {
      roles.push("canceled");
    }
    // If no specific role flags are provided, include all roles
    if (roles.length === 0) {
      roles = ["ordered", "preparing", "delivering", "received", "canceled"];
    }
    condition.$or = [...roles.map((value) => ({ status: value }))];
    // Initialize $or array for shippedDate or orderDate conditions
    const dateConditions = [];

    // Add condition for orderDate if it exists
    if (orderDate !== "null" && orderDate !== "NaN") {
      orderDate = formatDateAndTime(orderDate);
      if (typeof orderDate === "string") {
        orderDate = new Date(orderDate);
      }
      const nextDay = new Date(orderDate.getTime() + 24 * 60 * 60 * 1000);
      dateConditions.push({ orderDate: { $gte: orderDate, $lt: nextDay } });
    }

    // Add condition for shippedDate if it exists
    if (shippedDate !== "null" && shippedDate !== "NaN") {
      shippedDate = formatDateAndTime(shippedDate);
      if (typeof shippedDate === "string") {
        shippedDate = new Date(shippedDate);
      }
      const nextDay = new Date(shippedDate.getTime() + 24 * 60 * 60 * 1000);
      dateConditions.push({ shippedDate: { $gte: shippedDate, $lt: nextDay } });
    }
    // if (dateConditions.length > 0 ) {
    //   // No searchContent, only use date conditions
    //   condition.$or = dateConditions;
    // }

    if (searchContent && dateConditions.length === 0) {
      var searchEmail = "";
      // Check if searchContent is a valid ObjectId
      if (mongoose.Types.ObjectId.isValid(searchContent)) {
        condition.$or = [{ _id: searchContent }];
      } else {
        // Check if searchContent is numeric
        if (!isNaN(searchContent)) {
          condition.$or = [{ cost: Number(searchContent) }];
        } else {
          searchEmail = searchContent.trim();
          // Case-insensitive search for email
          // condition.$or = [{ shippedDate: 0 }];
        }
      }
    } else if (searchContent && dateConditions.length > 0) {
      condition.$or = dateConditions;
      var searchEmail = "";
      // Check if searchContent is a valid ObjectId
      if (mongoose.Types.ObjectId.isValid(searchContent)) {
        condition._id = searchContent;
      } else {
        // Check if searchContent is numeric
        if (!isNaN(searchContent)) {
          condition.cost = Number(searchContent);
        } else {
          searchEmail = searchContent.trim();
          // Case-insensitive search for email
          // condition.$or = [{ 'customer.email': { $regex: new RegExp(searchContent, 'i') } }];
        }
      }
    } else if (searchContent === "" && dateConditions.length > 0) {
      condition.$or = dateConditions;
    }
    let orders;
    //lấy tất cả order
    if (searchEmail) {
      orders = await orderModel
        .find(condition)
        // .sort({ createdAt: -1 }) // Sắp xếp từ thời gian mới nhất đến cũ nhất
        .skip((page - 1) * limit)
        .populate({
          path: "customer",
          match: { email: { $regex: new RegExp(searchEmail, "i") } },
        });
      const filteredOrders = searchEmail
        ? orders.filter((order) => order.customer && order.customer.email)
        : orders;
      if (filteredOrders && filteredOrders.length > 0) {
        return res.status(200).json({
          message: "Lấy tất cả sp thành công",
          data: filteredOrders,
        });
      } else {
        return res.status(200).json({
          message: "Ko có orders nào trong bảng",
          data: filteredOrders,
        });
      }
    } else {
      orders = await orderModel
        .find(condition)
        // .sort({ createdAt: -1 }) // Sắp xếp từ thời gian mới nhất đến cũ nhất
        .limit(limit)
        .skip((page - 1) * limit)
        .populate({
          path: "customer",
        });
      if (orders && orders.length > 0) {
        return res.status(200).json({
          message: "Lấy tất cả sp thành công",
          data: orders,
        });
      } else {
        return res.status(200).json({
          message: "Ko có orders nào trong bảng",
          data: orders,
        });
      }
    }
    // }else{
    //   //lấy order theo khách hàng
    //   const ordersOfCustomerId = await customerModel.findById(customerid).populate("orders");

    //   if(ordersOfCustomerId){
    //     return res.status(200).json({
    //       message: "Lấy tất cả reviews của courses thành công",
    //       data: ordersOfCustomerId,
    //     })
    //   }else{
    //     return res.status(400).json({
    //       message: "CustomerID ko tìm được orders."
    //     })
    //   }

    // }
  } catch (error) {
    return res.status(500).json({
      message: "Đã xảy ra lỗi",
    });
  }
};
//tạo order mới
const createOrder = async (req, res) => {
  const {
    reqOrderDate,
    reqShippedDate,
    reqNote,
    reqCost,
    reqAddressDelivery,
    customerInfos,
  } = req.body;
  var condition = {};
  // if(!mongoose.Types.ObjectId.isValid(customerid)){
  //   res.status(400).json({
  //     message: "Id ko hợp lệ"
  //   })
  // }
  try {
    const newCustomer = {
      _id: new mongoose.Types.ObjectId(),
      fullName: customerInfos.fullName,
      phone: customerInfos.phone,
      email: customerInfos.email,
      address: customerInfos.address,
      city: customerInfos.city,
      state: customerInfos.state,
      country: customerInfos.country,
    };
    condition.$or = [
      // { phone: customerInfos.phone },
      { email: customerInfos.email },
    ];
    // Nếu số email/sdt của khách trùng với email/sdt  đã có trong DB thì
    // sẽ cập nhật thông tin khách hàng đã có với email/sdt đó và add thông tin order mới này cho khách hàng đó.
    const customerFound = await customerModel.find(condition);
    var orderCreated = "";
    var updatedCustomer = "";
    if (customerFound.length === 1) {
      const newItem = {
        _id: new mongoose.Types.ObjectId(),
        orderDate: reqOrderDate,
        shippedDate: reqShippedDate,
        note: reqNote,
        cost: reqCost,
        addressDelivery: reqAddressDelivery,
        customer: customerFound[0]._id,
      }; //tạo order mới cho customer cũ
      orderCreated = await orderModel.create(newItem);
      updatedCustomer = await customerModel.findOneAndUpdate(
        { _id: customerFound[0]._id },
        {
          $set: {
            // Cập nhật thông tin khách hàng vào khách hàng cũ
            fullName: newCustomer.fullName,
            phone: newCustomer.phone,
            email: newCustomer.email,
            address: newCustomer.address,
            state: newCustomer.state,
            city: newCustomer.city,
            country: newCustomer.country,
          },
          $push: {
            // Thêm ID của đơn hàng vào mảng orders của khách hàng
            orders: orderCreated._id,
          },
        },
        {
          // Tùy chọn để trả về tài liệu được cập nhật mới
          new: true,
        }
      );
      if (orderCreated) {
        return res.status(201).json({
          message: "Create Order Successfully",
          data: await orderCreated,
          customer: await updatedCustomer,
          note: "old client",
        });
      }
    } else {
      // Nếu email và sdt không trùng thì tạo mới khách hàng và order mới trong DB
      const customerCreated = await customerModel.create(newCustomer);
      const newItemWithNewCustomerId = {
        _id: new mongoose.Types.ObjectId(),
        orderDate: reqOrderDate,
        shippedDate: reqShippedDate,
        note: reqNote,
        cost: reqCost,
        addressDelivery: reqAddressDelivery,
        //thêm idCustomer cho customer mới khi tạo order mới
        customer: customerCreated._id,
      };
      //tạo khách hàng mới và order mới
      const orderCreated = await orderModel.create(newItemWithNewCustomerId);
      const user = await userModel.findOneAndUpdate(
        { email: customerInfos.email },
        { customer: customerCreated._id },
        { new: true } // update customer id in customer field of user model
      );
      //update orders mới vào fields orders (là 1 array) vào customer vừa tạo
      if (orderCreated) {
        updatedCustomer = await customerModel.findByIdAndUpdate(
          customerCreated._id,
          {
            $push: { orders: orderCreated._id },
          }
        );
      }
      if (orderCreated) {
        return res.status(201).json({
          message: "Create Order Successfully",
          data: await orderCreated,
          customer: await updatedCustomer,
          note: "nope",
        });
      }
    }
  } catch (error) {
    if (error.code === 11000 || error.code === 11001) {
      // Xử lý lỗi duplicate key error
      return res.status(500).json({
        message: "Duplicate phone",
      });
      // Thực hiện các hành động cần thiết khi xảy ra lỗi duplicate key
    } else {
      return res.status(500).json({
        message: "Have an error",
      });
    }
  }
};
//lấy order theo id
const getOrderById = async (req, res) => {
  const orderid = req.params.orderid;
  if (!mongoose.Types.ObjectId.isValid(orderid)) {
    return res.status(400).json({
      message: "ID nhập vào ko hợp lệ",
    });
  }
  try {
    const result = await orderModel.findById(orderid).populate("customer");
    if (result) {
      return res.status(200).json({
        message: "Lấy thông tin theo id thành công",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "Id ko hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra",
    });
  }
};
//cập nhật order
const updateOrder = async (req, res) => {
  const orderid = req.params.orderid;
  if (!mongoose.Types.ObjectId.isValid(orderid)) {
    return res.status(400).json({
      message: "ID truyền vào ko hợp lệ",
    });
  }
  const { reqOrderDate, reqShippedDate, reqNote, reqCost, reqStatus } =
    req.body;
  try {
    const newItemUpdate = {};
    if (reqOrderDate) {
      newItemUpdate.orderDate = reqOrderDate;
    }
    if (reqShippedDate) {
      newItemUpdate.shippedDate = reqShippedDate;
    }
    if (reqNote) {
      newItemUpdate.note = reqNote;
    }
    if (reqCost) {
      newItemUpdate.cost = reqCost;
    }
    if (reqStatus) {
      newItemUpdate.status = reqStatus;
    }
    const result = await orderModel.findByIdAndUpdate(orderid, newItemUpdate);
    if (result) {
      return res.status(200).json({
        message: "Order Updated Successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID chưa hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Have an error",
    });
  }
};
//xóa mềm order
const softDeleteOrder = async (req, res) => {
  const orderid = req.params.orderid;

  // Kiểm tra ID hợp lệ
  if (!mongoose.Types.ObjectId.isValid(orderid)) {
    return res.status(400).json({
      message: "ID không hợp lệ",
    });
  }

  try {
    // Tìm đơn hàng theo ID
    const order = await orderModel.findById(orderid);
    // Kiểm tra nếu đơn hàng không tồn tại
    if (!order) {
      return res.status(404).json({ message: "Order not found" });
    }
    // Kiểm tra trạng thái của đơn hàng
    if (order.status != "received" && order.status != "canceled") {
      return res.status(400).json({
        message: "Can not delete active orders",
        // status: order.status
      });
    }

    // Cập nhật deletedAt thành thời gian hiện tại để thực hiện soft delete
    order.deletedAt = new Date();
    await order.save();

    // Kiểm tra kết quả cập nhật
    res.status(200).json({
      message: "Order Deleted Successfully",
      data: order,
    });
  } catch (error) {
    res.status(500).json({
      message: "Have an error",
    });
  }
};
//lấy tất cả các order đã xóa mềm
const getAllOrdersDeleted = async (req, res) => {
  try {
    //get product in softDelete (deletedAt !== null)
    const result = await orderModel
      .find({ deletedAt: { $ne: null } })
      .populate("customer");
    return res.status(200).json({
      message: "Get all orders deleted successfully",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Have an error",
    });
  }
};
//restore những sp xóa mềm thành sp bình thường
const restoreSoftDeleteOrder = async (req, res) => {
  const orderid = req.params.orderid;

  // Kiểm tra ID hợp lệ
  if (!mongoose.Types.ObjectId.isValid(orderid)) {
    return res.status(400).json({
      message: "ID không hợp lệ",
    });
  }
  try {
    const orderRestored = await orderModel.findById(orderid);
    if (!orderRestored) {
      return res.status(404).json({ message: "order not found" });
    }

    orderRestored.deletedAt = null;
    await orderRestored.save();

    res.status(200).json({
      message: "Order restored successfully",
      data: orderRestored,
    });
  } catch (error) {
    res.status(500).json({ message: "have an error" });
  }
};
//xóa cứng order (chưa dùng)
const deleteOrder = async (req, res) => {
  const orderid = req.params.orderid;
  if (!mongoose.Types.ObjectId.isValid(orderid)) {
    res.status(400).json({
      message: "ID ko hợp lệ",
    });
  }
  // const customerid = req.query.customerid;
  try {
    const orderDeleted = await orderModel.findByIdAndDelete(orderid);
    // tìm xem trong bảng customer nào chứa order này
    const orderOfCustomer = await customerModel
      .findOne({ orders: orderid })
      .exec();
    //nếu có customer chứa order này thì lấy id của customer đó
    if (orderOfCustomer._id) {
      //xóa order này đồng thời ở bảng customer
      await customerModel.findByIdAndUpdate(orderOfCustomer._id, {
        $pull: { orders: orderid },
      });
    }
    if (orderDeleted) {
      return res.status(200).json({
        message: "Order Deleted Successfully",
        data: orderDeleted,
      });
    } else {
      return res.status(400).json({
        message: "ID ko hợp lệ",
      });
    }
  } catch (error) {
    res.json({
      message: "Have an error",
    });
  }
};
//lấy những order trong ngày hôm nay
const getOrderInDay = async (req, res) => {
  try {
    const today = new Date();
    const year = today.getFullYear();
    const month = String(today.getMonth() + 1).padStart(2, "0"); // Months are zero-indexed in JS
    const day = String(today.getDate()).padStart(2, "0");
    const todayString = `${year}-${month}-${day}`;
    // Query to find orders with today's date
    const query = {
      orderDate: {
        $gte: new Date(`${todayString}T00:00:00.000Z`),
        $lt: new Date(`${todayString}T23:59:59.999Z`),
      },
    };

    const todayOrders = await orderModel.find(query);

    // return results;
    if (todayOrders) {
      return res.status(200).json({
        message: "get all order in this day successfully",
        data: todayOrders,
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "An error occurred",
      error: error,
    });
  }
};
//lấy những order theo customer id
const getOrdersOfCustomer = async (req, res) => {
  const customerid = req.query.customerid;
  if (customerid && !mongoose.Types.ObjectId.isValid(customerid)) {
    res.status(400).json({
      message: "Id ko hợp lệ",
    });
  }
  var limit = req.query.limit;
  if (limit == "null") {
    limit = null;
  }
  var page = req.query.page;
  const searchContent = req.query.search;
  var shippedDate = req.query.shippedDate || "null";
  var orderDate = req.query.orderDate || "null";
  var ordered = req.query.ordered;
  var preparing = req.query.preparing;
  var received = req.query.received;
  var canceled = req.query.canceled;
  var delivered = req.query.delivered;

  const formatDateAndTime = (dateAndTime) => {
    // Chuyển đổi chuỗi thành số
    const timestamp = parseInt(dateAndTime, 10);
    // Tạo đối tượng Date từ timestamp
    const date = new Date(timestamp);
    // Chuyển đổi thành chuỗi ISO 8601
    const isoDateString = date.toISOString();
    return isoDateString;
  };

  try {
    const condition = {};
    if (customerid) {
      condition.customer = customerid;
    }

    let roles = [];
    if (ordered === "true") {
      roles.push("ordered");
    }
    if (preparing === "true") {
      roles.push("preparing");
    }
    if (delivered === "true") {
      roles.push("delivering");
    }
    if (received === "true") {
      roles.push("received");
    }
    if (canceled === "true") {
      roles.push("canceled");
    }
    // If no specific role flags are provided, include all roles
    if (roles.length === 0) {
      roles = ["ordered", "preparing", "delivering", "received", "canceled"];
    }
    condition.$or = [...roles.map((value) => ({ status: value }))];
    // Initialize $or array for shippedDate or orderDate conditions
    const dateConditions = [];

    // Add condition for orderDate if it exists
    if (orderDate !== "null" && orderDate !== "NaN") {
      orderDate = formatDateAndTime(orderDate);
      if (typeof orderDate === "string") {
        orderDate = new Date(orderDate);
      }
      const nextDay = new Date(orderDate.getTime() + 24 * 60 * 60 * 1000);
      dateConditions.push({ orderDate: { $gte: orderDate, $lt: nextDay } });
    }

    // Add condition for shippedDate if it exists
    if (shippedDate !== "null" && shippedDate !== "NaN") {
      shippedDate = formatDateAndTime(shippedDate);
      if (typeof shippedDate === "string") {
        shippedDate = new Date(shippedDate);
      }
      const nextDay = new Date(shippedDate.getTime() + 24 * 60 * 60 * 1000);
      dateConditions.push({ shippedDate: { $gte: shippedDate, $lt: nextDay } });
    }
    // if (dateConditions.length > 0 ) {
    //   // No searchContent, only use date conditions
    //   condition.$or = dateConditions;
    // }

    if (searchContent && dateConditions.length === 0) {
      var searchEmail = "";
      // Check if searchContent is a valid ObjectId
      if (mongoose.Types.ObjectId.isValid(searchContent)) {
        condition.$or = [{ _id: searchContent }];
      } else {
        // Check if searchContent is numeric
        if (!isNaN(searchContent)) {
          condition.$or = [{ cost: Number(searchContent) }];
        } else {
          searchEmail = searchContent.trim();
          // Case-insensitive search for email
          // condition.$or = [{ shippedDate: 0 }];
        }
      }
    } else if (searchContent && dateConditions.length > 0) {
      condition.$or = dateConditions;
      var searchEmail = "";
      // Check if searchContent is a valid ObjectId
      if (mongoose.Types.ObjectId.isValid(searchContent)) {
        condition._id = searchContent;
      } else {
        // Check if searchContent is numeric
        if (!isNaN(searchContent)) {
          condition.cost = Number(searchContent);
        } else {
          searchEmail = searchContent.trim();
          // Case-insensitive search for email
          // condition.$or = [{ 'customer.email': { $regex: new RegExp(searchContent, 'i') } }];
        }
      }
    } else if (searchContent === "" && dateConditions.length > 0) {
      condition.$or = dateConditions;
    }
    let orders;
    //lấy tất cả order
    if (searchEmail) {
      orders = await orderModel
        .find(condition)
        // .sort({ createdAt: -1 })
        .skip((page - 1) * limit)
        .populate({
          path: "customer",
          match: { email: { $regex: new RegExp(searchEmail, "i") } },
        });
      const filteredOrders = searchEmail
        ? orders.filter((order) => order.customer && order.customer.email)
        : orders;
      if (filteredOrders && filteredOrders.length > 0) {
        return res.status(200).json({
          message: "Lấy tất cả sp thành công",
          data: filteredOrders,
        });
      } else {
        return res.status(200).json({
          message: "Ko có orders nào trong bảng",
          data: filteredOrders,
        });
      }
    } else {
      orders = await orderModel
        .find(condition)
        // .sort({ createdAt: -1 })
        .limit(limit)
        .skip((page - 1) * limit)
        .populate({
          path: "customer",
        });
      if (orders && orders.length > 0) {
        return res.status(200).json({
          message: "Lấy tất cả sp thành công",
          data: orders,
        });
      } else {
        return res.status(200).json({
          message: "Ko có orders nào trong bảng",
          data: orders,
        });
      }
    }
    // }else{
    //   //lấy order theo khách hàng
    //   const ordersOfCustomerId = await customerModel.findById(customerid).populate("orders");

    //   if(ordersOfCustomerId){
    //     return res.status(200).json({
    //       message: "Lấy tất cả reviews của courses thành công",
    //       data: ordersOfCustomerId,
    //     })
    //   }else{
    //     return res.status(400).json({
    //       message: "CustomerID ko tìm được orders."
    //     })
    //   }

    // }
  } catch (error) {
    return res.status(500).json({
      message: "Đã xảy ra lỗi",
    });
  }
};

module.exports = {
  getAllOrder,
  createOrder,
  getOrderById,
  updateOrder,
  deleteOrder,
  getOrderInDay,
  restoreSoftDeleteOrder,
  getAllOrdersDeleted,
  softDeleteOrder,
  exportOrdersToExcel,
  getOrdersOfCustomer,
};
