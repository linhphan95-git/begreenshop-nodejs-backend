const db = require("../models");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const refreshTokenModel = require("../models/refreshToken.model");
const refreshTokenService = require("../services/refreshToken.service");
const nodemailer = require("nodemailer");
const { OAuth2Client } = require("google-auth-library");

//kiểm tra email trước khi đăng ký xem email có tồn tại ko?
const verifyEmailToSignUp = async (req, res) => {
  const { email } = req.body;
  try {
    // // Kiểm tra email đã tồn tại chưa
    // const existingUser = await db.user.findOne({ email });
    // if (existingUser) {
    //     return res.status(400).json({ message: "Email already in use" });
    // }

    // Tạo mã xác thực
    const verificationCode = crypto.randomBytes(20).toString("hex");
    const secretKey = process.env.JWT_SECRET_KEY;
    // Lưu mã xác thực vào cơ sở dữ liệu tạm thời
    const token = jwt.sign({ email, verificationCode }, secretKey, {
      expiresIn: "1h",
    });

    // Khởi tạo OAuth2Client với Client ID và Client Secret
    const myOAuth2Client = new OAuth2Client(
      process.env.GOOGLE_MAILER_CLIENT_ID,
      process.env.GOOGLE_MAILER_CLIENT_SECRET
    );
    // Set Refresh Token vào OAuth2Client Credentials
    myOAuth2Client.setCredentials({
      refresh_token: process.env.GOOGLE_MAILER_REFRESH_TOKEN,
    });
    const myAccessTokenObject = await myOAuth2Client.getAccessToken();
    // Access Token sẽ nằm trong property 'token' trong Object mà chúng ta vừa get được ở trên
    const myAccessToken = myAccessTokenObject?.token;
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: process.env.ADMIN_EMAIL_ADDRESS,
        clientId: process.env.GOOGLE_MAILER_CLIENT_ID,
        clientSecret: process.env.GOOGLE_MAILER_CLIENT_SECRET,
        refresh_token: process.env.GOOGLE_MAILER_REFRESH_TOKEN,
        accessToken: myAccessToken,
      },
    });
    // Gửi email
    const mailOptions = {
      from: "no-reply@yourdomain.com",
      to: email,
      subject: "Email Verification",
      text: `Your verification code is: ${verificationCode}`,
    };

    await transport.sendMail(mailOptions);

    res.status(200).json({ message: "Verification email sent", token });
  } catch (error) {
    res.status(500).json({ message: "Internal server error" });
  }
};
//hàm đăng kí tk mới
const signUp = async (req, res) => {
  try {
    const secretKey = process.env.JWT_SECRET_KEY;
    const decoded = jwt.verify(req.body.token, secretKey);
    //kiểm tra mã verification của email xem email có tồn tại ko
    if (decoded.verificationCode !== req.body.verificationCode) {
      return res.status(400).json({ message: "Invalid verification code" });
    }

    //tìm id của role truyền vào tương ứng
    const userRole = await db.role.findOne({
      name: req.body.role, //mặc định role ban đầu là user
    });
    const user = new db.user({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
      //cập nhật roleId vào roles
      roles: [userRole._id],
    });
    await user.save();

    res.status(200).json({
      message: "Create user successfully",
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal server error",
    });
  }
};
//hàm đăng nhập
const logIn = async (req, res) => {
  try {
    const existUser = await db.user
      .findOne({
        username: req.body.username,
      })
      .populate("roles");
    if (!existUser) {
      return res.status(404).send({
        message: "User not found",
      });
    }
    if (existUser && existUser.deletedAt !== null) {
      return res.status(404).send({
        message: "User Account is inactive",
      });
    }
    var passwordIsValid = bcrypt.compareSync(
      req.body.password,
      existUser.password
    );

    if (!passwordIsValid) {
      return res.status(401).json({
        message: "Invalid password",
      });
    }

    const secretKey = process.env.JWT_SECRET_KEY;
    const expiresTime = process.env.JWT_EXPIRES_TIME;
    const token = jwt.sign({ id: existUser._id }, secretKey, {
      algorithm: "HS256",
      allowInsecureKeySizes: true,
      expiresIn: parseInt(expiresTime),
    });
    //sinh thêm refresh token
    const refreshToken = await refreshTokenService.createToken(existUser);
    res.status(200).json({
      accessToken: token,
      refreshToken: refreshToken,
      email: existUser.email,
      roles: existUser.roles,
    });
  } catch (error) {
    res.status(500).json({
      message: "Intenal server error",
    });
  }
};

//hàm tạo lại refreshtoken mới
const refreshToken = async (req, res) => {
  //điền refreshtoken được tạo khi signin vào body param
  const { refreshToken } = req.body;
  //nếu null
  if (refreshToken == null) {
    return res.status(403).json({
      message: "Refresh token is required",
    });
  }

  try {
    //nếu tồn tại thì kiểm tra xem refreshToken này có tồn tại trong obj đã lưu
    //khi tạo refreshtoken lúc signin chưa
    const refreshTokenObj = await refreshTokenModel.findOne({
      token: refreshToken,
    });
    //nếu ko có thì báo 403
    if (!refreshTokenObj) {
      return res.status(403).json({
        message: "Refresh token not found",
      });
    }
    //nếu có thì so sánh xem thời gian hết hạn của nó bé hơn thời gian
    //hiện tại hay ko? sau đó xóa nó ra khỏi obj
    if (refreshTokenObj.expiredDate.getTime() < Date.now()) {
      await refreshTokenModel.findByIdAndRemove(refreshTokenObj._id);
      return res.status(403).json({
        message: "Refresh token expired",
      });
    }
    //tạo 1 access token mới
    const secretKey = process.env.JWT_SECRET_KEY;
    const expiresTime = process.env.JWT_EXPIRES_TIME;
    const newAccessToken = jwt.sign(
      {
        id: refreshTokenObj.user,
      },
      secretKey,
      {
        expiresIn: parseInt(expiresTime),
      }
    );
    return res.status(200).json({
      accessToken: newAccessToken,
      refreshToken: refreshTokenObj.token,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Internal Server Error",
    });
  }
};
//hàm xử lý quên password
const forgotPassword = async (req, res) => {
  const { email } = req.body;
  // Khởi tạo OAuth2Client với Client ID và Client Secret
  const myOAuth2Client = new OAuth2Client(
    process.env.GOOGLE_MAILER_CLIENT_ID,
    process.env.GOOGLE_MAILER_CLIENT_SECRET
  );
  // Set Refresh Token vào OAuth2Client Credentials
  myOAuth2Client.setCredentials({
    refresh_token: process.env.GOOGLE_MAILER_REFRESH_TOKEN,
  });
  try {
    const user = await db.user.findOne({ email });
    if (!user) {
      return res.status(400).json({ message: "User not found" });
    }

    const secretKey = process.env.JWT_SECRET_KEY;
    const expiresTime = process.env.JWT_EXPIRES_TIME;
    const token = jwt.sign({ id: user._id }, secretKey, {
      algorithm: "HS256",
      allowInsecureKeySizes: true,
      expiresIn: parseInt(expiresTime),
    });

    user.resetPasswordToken = token;
    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
    await user.save();

    const myAccessTokenObject = await myOAuth2Client.getAccessToken();
    // Access Token sẽ nằm trong property 'token' trong Object mà chúng ta vừa get được ở trên
    const myAccessToken = myAccessTokenObject?.token;
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: process.env.ADMIN_EMAIL_ADDRESS,
        clientId: process.env.GOOGLE_MAILER_CLIENT_ID,
        clientSecret: process.env.GOOGLE_MAILER_CLIENT_SECRET,
        refresh_token: process.env.GOOGLE_MAILER_REFRESH_TOKEN,
        accessToken: myAccessToken,
      },
    });

    const mailOptions = {
      to: user.email,
      from: "passwordreset@demo.com",
      subject: "Password Reset",
      text: `Please copy this resetToken and paste it into the reset password form to complete the process:${token}
            If you did not request this, please ignore this email and your password will remain unchanged.`,
    };

    // Gọi hành động gửi email
    await transport.sendMail(mailOptions);
    // Không có lỗi gì thì trả về success
    res.status(200).json({ message: "Email sent successfully." });
  } catch (err) {
    res.status(500).json({ message: "Error sending reset password email" });
  }
};
//hàm xử lý reset password
const resetPassword = async (req, res) => {
  const { password } = req.body;
  const { token } = req.body;
  try {
    const user = await db.user.findOne({
      resetPasswordToken: token,
      resetPasswordExpires: { $gt: Date.now() },
    });

    if (!user) {
      return res
        .status(400)
        .json({ message: "Password reset token is invalid or has expired" });
    }

    // user.password = user.hashPassword(password);
    (user.password = bcrypt.hashSync(password, 8)),
      (user.resetPasswordToken = undefined);
    user.resetPasswordExpires = undefined;
    await user.save();

    res.status(200).json({ message: "Password has been reset" });
  } catch (err) {
    res.status(500).json({ message: "Error resetting password" });
  }
};
module.exports = {
  signUp,
  logIn,
  refreshToken,
  forgotPassword,
  resetPassword,
  verifyEmailToSignUp,
};
