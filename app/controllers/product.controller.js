const mongoose = require("mongoose");
const productModel = require("../models/product.model");
const productTypeModel = require("../models/productType.model");
const orderDetailModel = require("../models/orderDetail.model");
const orderModel = require("../models/order.model");
const { notifyProductInfosChange } = require("../../websocket"); // Adjust the path as needed
const XLSX = require("xlsx");
const fs = require("fs");

//hàm xuất thông tin bảng ra file excel
const exportProductsToExcel = async (req, res) => {
  try {
    const result = await productModel.find();
    if (result && result.length > 0) {
      const filePath = "products.xlsx";
      const wb = XLSX.utils.book_new();
      const jsonResult = result.map((product) => {
        const obj = product.toObject();
        obj._id = product._id.toString(); // Add id field explicitly
        return obj;
      });

      const ws = XLSX.utils.json_to_sheet(jsonResult);
      XLSX.utils.book_append_sheet(wb, ws, "Products");
      XLSX.writeFile(wb, filePath);

      res.download(filePath, (err) => {
        if (err) {
          // console.log(err);
        }
        // Xóa file sau khi gửi
        fs.unlinkSync(filePath);
      });
    } else {
      res.status(404).json({ message: "No products found" });
    }
  } catch (error) {
    res.status(500).json({ message: "Failed to fetch data" });
  }
};
//lấy tất cả các sp
const getAllProduct = async (req, res) => {
  const page = req.query.page;
  const limit = req.query.limit;
  const nameProductQuery = req.query.name;
  const minPriceQuery = req.query.min;
  const maxPriceQuery = req.query.max;
  const inStockQuery = req.query.inStock;
  const outStockQuery = req.query.outStock;
  const allStockQuery = req.query.allStock;
  const typePage = req.query.typePage;
  var typesArray = [];
  var condition = {};
  // var condiType = []
  if (req.query.typeId !== undefined) {
    const productTypeId = req.query.typeId;
    typesArray = productTypeId.split(",");
    condition.$or = [...typesArray.map((value) => ({ type: value }))];
  }

  function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
  }
  const escapedNameProductQuery = escapeRegExp(nameProductQuery);
  condition.name = { $regex: new RegExp(escapedNameProductQuery, "i") };
  //chuyển từ string về boolean
  const inStock = inStockQuery === "true";
  const outStock = outStockQuery === "true";
  const allStock = allStockQuery === "true";
  if (inStock && !outStock && !allStock) {
    condition.amount = { $gt: 0 }; // Chỉ đặt điều kiện cho amount khi chỉ có inStockQuery được kích hoạt
  }
  if (outStock && !inStock && !allStock) {
    condition.amount = { $lte: 0 }; // Chỉ đặt điều kiện cho amount khi chỉ có outStockQuery được kích hoạt
  }
  if (allStock || (inStock && outStock)) {
    condition.$or = [
      { amount: { $gt: 0 } }, // Chọn các mục có số lượng lớn hơn 0
      { amount: { $lte: 0 } },
    ];
  }
  if (minPriceQuery) {
    condition.buyPrice = { $gte: minPriceQuery };
  }
  if (maxPriceQuery) {
    condition.buyPrice = { ...condition.minPriceQuery, $lte: maxPriceQuery };
  }
  // lấy các sp ko nằm trong danh sách xóa mềm
  condition.deletedAt = null;

  try {
    let result = [];
    let totalPage = 0;
    // if (typePage !== "home") {
    result = await productModel
      .find(condition)
      //.sort({ createdAt: -1 })
      .limit(limit)
      .skip((page - 1) * limit)
      .populate("type");
    return res.status(200).json({
      message: "Lấy tất cả sp thành công",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Đã xảy ra lỗi",
    });
  }
};
//lấy những sp thuộc type chi tiết
const getProductsOfType = async (req, res) => {
  const limit = parseInt(req.query.limit, 10);
  const page = parseInt(req.query.page, 10);
  const nameProductQuery = req.query.name;
  const minPriceQuery = req.query.min;
  const maxPriceQuery = req.query.max;
  const inStockQuery = req.query.inStock;
  const outStockQuery = req.query.outStock;
  const allStockQuery = req.query.allStock;

  var condition = {};
  const productTypeId = req.params.productTypeId;
  condition.type = productTypeId;
  condition.deletedAt = null; //ko lấy những sp nằm trong bảng xóa mềm
  if (nameProductQuery) {
    //ko phân biệt hoa thường
    condition.name = { $regex: new RegExp(nameProductQuery, "i") };
  }
  //chuyển từ string về boolean
  const inStock = inStockQuery === "true";
  const outStock = outStockQuery === "true";
  const allStock = allStockQuery === "true";
  if (inStock && !outStock && !allStock) {
    condition.amount = { $gt: 0 }; // Chỉ đặt điều kiện cho amount khi chỉ có inStockQuery được kích hoạt
  }
  if (outStock && !inStock && !allStock) {
    condition.amount = { $lte: 0 }; // Chỉ đặt điều kiện cho amount khi chỉ có outStockQuery được kích hoạt
  }
  if (allStock || (inStock && outStock)) {
    condition.$or = [
      { amount: { $gt: 0 } }, // Chọn các mục có số lượng lớn hơn 0
      { amount: { $lte: 0 } },
    ];
  }
  if (minPriceQuery) {
    condition.buyPrice = { $gte: minPriceQuery };
  }
  if (maxPriceQuery) {
    condition.buyPrice = { ...condition.minPriceQuery, $lte: maxPriceQuery };
  }
  // if(!mongoose.Types.ObjectId.isValid(productTypeId)){
  //   return res.status(400).json({
  //     message: "productTypeId nhập vào ko hợp lệ"
  //   })
  // }
  try {
    const result = await productModel
      .find(condition)
      .limit(limit)
      .skip((page - 1) * limit);
    const typeProductDetail = await productTypeModel.findById(productTypeId);
    return res.status(200).json({
      message: "Lấy tất cả dữ liệu thành công",
      data: result,
      typeProductDetail: typeProductDetail,
    });
  } catch (error) {
    return res.status(500).json({
      message: "có lỗi xảy ra",
    });
  }
};
//tạo sp mới
const createProduct = async (req, res) => {
  const {
    reqName,
    reqDescription,
    reqType,
    reqImageUrl,
    reqBuyPrice,
    reqPromotionPrice,
    reqAmount,
    reqOrigin,
    reqWeight,
  } = req.body;
  if (!reqName) {
    return res.status(400).json({
      message: "Tên sp ko hợp lệ",
    });
  }
  if (!reqType) {
    return res.status(400).json({
      message: "Tên product type ko hợp lệ",
    });
  }
  if (!reqImageUrl) {
    return res.status(400).json({
      message: "Đường dẫn hình ảnh ko hợp lệ",
    });
  }
  // if(!reqBuyPrice || !Number.isInteger(reqBuyPrice) || reqBuyPrice<0){
  //   return res.status(400).json({
  //     message: "Giá sp ko hợp lệ"
  //   })
  // }
  //price must be a number and greater than 0
  if (isNaN(parseFloat(reqBuyPrice)) || parseFloat(reqBuyPrice) <= 0) {
    return res.status(400).json({
      message: "Giá sp ko hợp lệ",
    });
  }
  if (
    isNaN(parseFloat(reqPromotionPrice)) ||
    parseFloat(reqPromotionPrice) < 0
  ) {
    return res.status(400).json({
      message: "Giá khuyến mãi sp ko hợp lệ",
    });
  }
  if (isNaN(parseFloat(reqAmount)) || parseFloat(reqAmount) < 0) {
    return res.status(400).json({
      message: "Số lượng sp ko hợp lệ",
    });
  }
  if (isNaN(parseFloat(reqWeight)) || parseFloat(reqWeight) <= 0) {
    return res.status(400).json({
      message: "Khối lượng sp ko hợp lệ",
    });
  }
  try {
    const newItem = {
      _id: new mongoose.Types.ObjectId(),
      name: reqName,
      description: reqDescription,
      type: reqType,
      imageUrl: reqImageUrl,
      buyPrice: reqBuyPrice,
      promotionPrice: reqPromotionPrice,
      amount: reqAmount,
      origin: reqOrigin,
      weight: reqWeight,
    };
    const result = await productModel.create(newItem);
    return res.status(201).json({
      message: "Create Successfully",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Have an error " + error,
    });
  }
};
//lấy sp theo id
const getProductById = async (req, res) => {
  const productid = req.params.productid;
  if (!mongoose.Types.ObjectId.isValid(productid)) {
    return res.status(400).json({
      message: "ID nhập vào ko hợp lệ",
    });
  }
  try {
    const result = await productModel.findById(productid).populate("type");
    const typeProduct = await productModel
      .findById(productid)
      .populate("type", "name");
    if (result) {
      return res.status(200).json({
        message: "Lấy thông tin theo id thành công",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "Id ko hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra",
    });
  }
};
//chỉnh sửa sp
const updateProduct = async (req, res) => {
  const productid = req.params.productid;
  if (!mongoose.Types.ObjectId.isValid(productid)) {
    return res.status(400).json({
      message: "ID truyền vào ko hợp lệ",
    });
  }
  const {
    name,
    description,
    type,
    imageUrl,
    buyPrice,
    promotionPrice,
    amount,
    origin,
    weight,
  } = req.body;
  if (name == "") {
    return res.status(400).json({
      message: "Tên sp ko hợp lệ",
    });
  }
  if (type == "") {
    return res.status(400).json({
      message: "Mã product type ko hợp lệ",
    });
  }
  if (imageUrl == "") {
    return res.status(400).json({
      message: "Đường dẫn hình ảnh ko hợp lệ",
    });
  }
  //nếu giá sp được điền (#undefined) nếu nó là "" hoặc <=0 thì sai
  if (
    (buyPrice !== undefined && isNaN(parseFloat(buyPrice))) ||
    parseFloat(buyPrice) <= 0
  ) {
    return res.status(400).json({
      message: "Giá sp ko hợp lệ",
    });
  }
  if (
    (promotionPrice !== undefined && isNaN(parseFloat(promotionPrice))) ||
    parseFloat(promotionPrice) < 0
  ) {
    return res.status(400).json({
      message: "Giá khuyến mãi sp ko hợp lệ",
    });
  }
  if (
    (amount !== undefined && isNaN(parseFloat(amount))) ||
    parseFloat(amount) < 0
  ) {
    return res.status(400).json({
      message: "Số lượng sp ko hợp lệ",
    });
  }
  if (origin == "") {
    return res.status(400).json({
      message: "reqOrigin ko hợp lệ",
    });
  }
  if (
    (weight !== undefined && isNaN(parseFloat(weight))) ||
    parseFloat(weight) <= 0
  ) {
    return res.status(400).json({
      message: "Khối lượng sp ko hợp lệ",
    });
  }
  try {
    const newItemUpdate = {};
    if (name) {
      newItemUpdate.name = name;
    }
    if (description) {
      newItemUpdate.description = description;
    }
    if (type) {
      newItemUpdate.type = type;
    }
    if (imageUrl) {
      newItemUpdate.imageUrl = imageUrl;
    }
    if (buyPrice) {
      newItemUpdate.buyPrice = buyPrice;
    }
    if (promotionPrice) {
      newItemUpdate.promotionPrice = promotionPrice;
    }
    if (amount) {
      newItemUpdate.amount = amount;
    }
    if (weight) {
      newItemUpdate.weight = weight;
    }
    if (origin) {
      newItemUpdate.origin = origin;
    }
    const result = await productModel.findByIdAndUpdate(
      productid,
      newItemUpdate
    );
    notifyProductInfosChange(); // Gửi thông báo WebSocket
    if (result) {
      return res.status(200).json({
        message: "Edit Successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID chưa hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Have an error " + error,
    });
  }
};
//xóa sp
const deleteProduct = async (req, res) => {
  const productid = req.params.productid;
  if (!mongoose.Types.ObjectId.isValid(productid)) {
    res.status(400).json({
      message: "ID ko hợp lệ",
    });
  }
  try {
    const result = await productModel.findByIdAndDelete(productid);
    if (result) {
      return res.status(200).json({
        message: "Delete Successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID ko hợp lệ",
      });
    }
  } catch (error) {
    res.json({
      message: "Have an error",
    });
  }
};
//xóa mềm 1 sp
const softDeleteProduct = async (req, res) => {
  const productid = req.params.productid;
  if (!mongoose.Types.ObjectId.isValid(productid)) {
    res.status(400).json({
      message: "ID ko hợp lệ",
    });
  }
  try {
    const product = await productModel.findById(productid);
    if (!productid) {
      return res.status(404).json({ message: "productid not found" });
    }
    // Find order details that include this product
    const orderDetails = await orderDetailModel.find({ product: productid });

    if (orderDetails.length > 0) {
      // tìm những order chứa orderDetail có đơn hàng đang active
      const orderIds = orderDetails.map((detail) => detail._id);
      const orders = await orderModel.find({
        orderDetails: { $in: orderIds },
        //status khác "received", "canceled"
        status: { $nin: ["received", "canceled"] },
      });

      if (orders.length > 0) {
        return res
          .status(400)
          .json({ message: "Cannot delete product with active orders" });
      }
    }
    //nếu ko thì xóa bình thường
    await product.softDelete();
    res.status(200).json({ message: "product soft deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: "Have an error" });
  }
};
//lấy tất cả những sp đã xóa
const getAllProductDeleted = async (req, res) => {
  try {
    //get product in softDelete (deletedAt !== null)
    const result = await productModel.find({ deletedAt: { $ne: null } });
    return res.status(200).json({
      message: "Get all product deleted successfully",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Have an error",
    });
  }
};
//restore những sp đã xóa mềm
const restoreSoftDelete = async (req, res) => {
  const productid = req.params.productid;
  if (!mongoose.Types.ObjectId.isValid(productid)) {
    res.status(400).json({
      message: "ID ko hợp lệ",
    });
  }
  try {
    const productRestored = await productModel.findById(productid);
    if (!productRestored) {
      return res.status(404).json({ message: "product not found" });
    }

    await productRestored.restore();
    res.status(200).json({ message: "product restored successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
//lấy những sp có orderDetail từ lớn đến nhỏ
const getFeaturedProducts = async (req, res) => {
  try {
    const results = await orderDetailModel.aggregate([
      // Nhóm theo product và tính tổng quantity
      {
        $group: {
          _id: "$product",
          totalQuantity: { $sum: "$quantity" },
        },
      },
      // Sắp xếp theo totalQuantity giảm dần
      {
        $sort: { totalQuantity: -1 },
      },
      // // Giới hạn số lượng sản phẩm nếu cần, ví dụ: top 10 sản phẩm
      // { $limit: 6 },
      // Join với bảng sản phẩm
      {
        $lookup: {
          from: "products",
          localField: "_id",
          foreignField: "_id",
          as: "productInfo",
        },
      },
      // Join với bảng product_types
      {
        $lookup: {
          from: "product_types",
          localField: "productInfo.type",
          foreignField: "_id",
          as: "typeInfo",
        },
      },
      // Giữ lại những sản phẩm có deletedAt == null trong productInfo và typeInfo
      {
        $match: {
          "productInfo.deletedAt": null,
          "typeInfo.deletedAt": null,
        },
      },
    ]);

    // return results;
    if (results) {
      return res.status(200).json({
        message: "get all featured successfully",
        data: results,
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "An error occurred",
      error: error,
    });
  }
};
module.exports = {
  getAllProduct,
  createProduct,
  getProductById,
  updateProduct,
  deleteProduct,
  getProductsOfType,
  softDeleteProduct,
  restoreSoftDelete,
  getAllProductDeleted,
  getFeaturedProducts,
  exportProductsToExcel,
};
