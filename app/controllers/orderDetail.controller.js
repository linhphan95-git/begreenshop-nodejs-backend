const mongoose = require("mongoose");
const orderDetailModel = require("../models/orderDetail.model");
const orderModel = require("../models/order.model");
const productModel = require("../models/product.model");
//lấy tất cả những order detail
const getAllOrderDetail = async (req, res) => {
  const orderid = req.query.orderid;
  if (orderid && !mongoose.Types.ObjectId.isValid(orderid)) {
    res.status(400).json({
      message: "Id ko hợp lệ",
    });
  }
  try {
    if (!orderid) {
      const result = await orderDetailModel.find();
      if (result && result.length > 0) {
        return res.status(200).json({
          message: "Lấy tất cả sp thành công",
          data: result,
        });
      } else {
        return res.status(200).json({
          message: "Ko có orderDetail nào trong bảng",
          data: result,
        });
      }
    } else {
      const orderDetailsOfOrderId = await orderModel
        .findById(orderid)
        .populate("orderDetails");
      if (orderDetailsOfOrderId) {
        return res.status(200).json({
          message: "Lấy tất cả orderDetails của orders thành công",
          data: orderDetailsOfOrderId,
        });
      } else {
        return res.status(400).json({
          message: "orderID ko tìm được orders.",
        });
      }
    }
  } catch (error) {
    return res.status(500).json({
      message: "Đã xảy ra lỗi",
    });
  }
};
//tạo order detail khi order được tạo
const createOrderDetail = async (req, res) => {
  const { reqProduct, reqQuantity, orderid } = req.body;
  if (reqQuantity < 0) {
    return res.status(400).json({
      message: "Số lượng chưa đúng",
    });
  }
  try {
    const newItem = {
      _id: new mongoose.Types.ObjectId(),
      quantity: reqQuantity,
      product: reqProduct,
    };
    const orderDetailCreated = await orderDetailModel.create(newItem);
    const updatedOrder = await orderModel.findByIdAndUpdate(orderid, {
      $push: { orderDetails: orderDetailCreated._id },
    });
    //cập nhật số lượng stock của sản phẩm sau khi đặt hàng
    const product = await productModel.findByIdAndUpdate(reqProduct);
    product.amount -= reqQuantity;
    await product.save();

    return res.status(201).json({
      message: " Tạo mới thành công",
      data: orderDetailCreated,
      order: updatedOrder,
      product: product,
    });
  } catch (error) {
    return res.status(500).json({
      message: "có lỗi xảy ra",
    });
  }
};
//lấy order detail theo id
const getOrderDetailById = async (req, res) => {
  const orderdetailId = req.params.orderdetailId;
  if (!mongoose.Types.ObjectId.isValid(orderdetailId)) {
    return res.status(400).json({
      message: "ID nhập vào ko hợp lệ",
    });
  }
  try {
    const result = await orderDetailModel
      .findById(orderdetailId)
      .populate("product");
    if (result) {
      return res.status(200).json({
        message: "Lấy thông tin theo id thành công",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "Id ko hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra",
    });
  }
};
//chỉnh sửa order chi tiết
const updateOrderDetail = async (req, res) => {
  const orderdetailId = req.params.orderdetailId;
  if (!mongoose.Types.ObjectId.isValid(orderdetailId)) {
    return res.status(400).json({
      message: "ID truyền vào ko hợp lệ",
    });
  }
  const { reqProduct, reqQuantity } = req.body;
  if (reqQuantity < 0) {
    return res.status(400).json({
      message: "Số lượng chưa đúng thành công",
    });
  }
  try {
    const newItemUpdate = {};
    if (reqQuantity) {
      newItemUpdate.quantity = reqQuantity;
    }
    if (reqProduct) {
      newItemUpdate.product = reqProduct;
    }
    const result = await orderDetailModel.findByIdAndUpdate(
      orderdetailId,
      newItemUpdate
    );
    if (result) {
      return res.status(200).json({
        message: "Cập nhật thành công",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID chưa hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra",
    });
  }
};
//xóa order chi tiết
const deleteOrderDetail = async (req, res) => {
  const orderdetailId = req.params.orderdetailId;
  if (!mongoose.Types.ObjectId.isValid(orderdetailId)) {
    res.status(400).json({
      message: "ID ko hợp lệ",
    });
  }
  const orderid = req.query.orderid;
  try {
    const orderDetailDeleted = await orderDetailModel.findByIdAndDelete(
      orderdetailId
    );
    if (orderid) {
      await orderModel.findByIdAndUpdate(orderid, {
        $pull: { orderDetails: orderdetailId },
      });
    }
    if (orderDetailDeleted) {
      return res.status(200).json({
        message: "Xóa thành công",
        data: orderDetailDeleted,
      });
    } else {
      return res.status(400).json({
        message: "ID ko hợp lệ",
      });
    }
  } catch (error) {
    res.json({
      message: "Có lỗi xảy ra",
    });
  }
};
//lấy sp trong orderDetail ra
const getProductsOfOrderDetail = async (req, res) => {
  const productId = req.params.productId;
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      message: "ID nhập vào ko hợp lệ",
    });
  }
  const condition = {};
  condition.product = productId;
  try {
    const result = await orderDetailModel.find(condition);
    if (result) {
      return res.status(200).json({
        message: "Lấy thông sp theo orderDetail thành công",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "Id ko hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra",
    });
  }
};

module.exports = {
  getAllOrderDetail,
  createOrderDetail,
  getOrderDetailById,
  updateOrderDetail,
  deleteOrderDetail,
  getProductsOfOrderDetail,
};
