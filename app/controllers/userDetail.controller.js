const mongoose = require("mongoose");
const customerModel = require("../models/customer.model");
const userModel = require("../models/user.model");

// const getAllUserDetail = async (req, res) => {
//   try {
//     // const result = await customerModel.find();
//     // console.log(result);
//     return res.status(200).json({
//       message: "Lấy tất cả userDetail thành công",
//       // data: result
//     })
//   } catch (error) {
//     return res.status(500).json({
//       message: "Đã xảy ra lỗi"
//     })
//   }
// }
//tạo user detail mới
const createUserDetail = async (req, res) => {
  const {
    reqFullName,
    reqPhone,
    reqEmail,
    reqAddress,
    reqCity,
    reqCountry,
    reqUserId,
  } = req.body;
  if (!reqFullName) {
    return res.status(400).json({
      message: "Tên sp ko hợp lệ",
    });
  }
  if (!reqPhone) {
    return res.status(400).json({
      message: "Tên product type ko hợp lệ",
    });
  }
  try {
    const newItem = {
      _id: new mongoose.Types.ObjectId(),
      fullName: reqFullName,
      phone: reqPhone,
      email: reqEmail,
      address: reqAddress,
      city: reqCity,
      country: reqCountry,
    };
    const result = await userDetailModel.create(newItem);
    const updatedCustomer = await userModel.findByIdAndUpdate(reqUserId, {
      $push: { userDetail: result._id },
    });

    return res.status(201).json({
      message: " Tạo mới thành công",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "có lỗi xảy ra",
    });
  }
};

// const getCustomerById = async (req,res) =>{
//   const customerid = req.params.customerid;
//   if(!mongoose.Types.ObjectId.isValid(customerid)){
//     return res.status(400).json({
//       message: "ID nhập vào ko hợp lệ"
//     })
//   }
//   try {
//     const result = await customerModel.findById(customerid);
//     if(result){
//       return res.status(200).json({
//         message: "Lấy thông tin theo id thành công",
//         data: result
//       })
//     }else{
//       return res.status(400).json({
//         message: "Id ko hợp lệ"
//       })
//     }
//   } catch (error) {
//     return res.status(500).json({
//       message: "Có lỗi xảy ra"
//     })
//   }
// }
// const updateCustomer  = async (req,res) =>{
//   const customerid = req.params.customerid;
//   if(!mongoose.Types.ObjectId.isValid(customerid)){
//     return res.status(400).json({
//       message: "ID truyền vào ko hợp lệ"
//     })
//   }
//   const {
//     reqFullName,
//     reqPhone,
//     reqEmail,
//     reqAddress,
//     reqCity,
//     reqCountry
//   } = req.body;
//   // console.log(req.body);
//   if(reqFullName == ""){
//     return res.status(400).json({
//       message: "Tên sp ko hợp lệ"
//     })
//   }
//   if(reqPhone == ""){
//     return res.status(400).json({
//       message: "Điện thoại ko hợp lệ"
//     })
//   }
//   if(reqEmail == ""){
//     return res.status(400).json({
//       message: "Email ko hợp lệ"
//     })
//   }
//   try {
//     const newItemUpdate = {};
//     if(reqFullName){
//       newItemUpdate.fullName = reqFullName;
//     }
//     if(reqPhone){
//       newItemUpdate.phone = reqPhone;
//     }
//     if(reqEmail){
//       newItemUpdate.email = reqEmail;
//     }
//     if(reqAddress){
//       newItemUpdate.address = reqAddress;
//     }
//     if(reqCity){
//       newItemUpdate.city = reqCity;
//     }
//     if(reqCountry){
//       newItemUpdate.country = reqCountry;
//     }
//     const result = await customerModel.findByIdAndUpdate(customerid,newItemUpdate);
//     if(result){
//       return res.status(200).json({
//         message: "Cập nhật thành công",
//         data: result
//       })
//     }else{
//       return res.status(400).json({
//         message: "ID chưa hợp lệ"
//       })
//     }
//   } catch (error) {
//     return res.status(500).json({
//       message: "Có lỗi xảy ra"
//     })
//   }
// }
// const deleteCustomer  = async (req,res) =>{
//   const customerid = req.params.customerid;
//   if(!mongoose.Types.ObjectId.isValid(customerid)){
//       res.status(400).json({
//       message: "ID ko hợp lệ"
//     })
//   }
//   try {
//     const result = await customerModel.findByIdAndDelete(customerid);
//     if(result){
//       return res.status(200).json({
//         message: "Xóa thành công",
//         data: result
//       })
//     }else{
//       return res.status(400).json({
//         message: "ID ko hợp lệ"
//       })
//     }
//   } catch (error) {
//      res.json({
//       message: "Có lỗi xảy ra"
//     })
//   }
// }
module.exports = {
  createUserDetail,
};
