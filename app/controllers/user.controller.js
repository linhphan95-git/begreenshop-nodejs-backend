const mongoose = require("mongoose");
const userModel = require("../models/user.model");
const bcrypt = require("bcrypt");
const orderModel = require("../models/order.model");
const orderDetailModel = require("../models/orderDetail.model");
const XLSX = require("xlsx");
const fs = require("fs");

//hàm xuất thông tin bảng ra file excel
const exportUsersToExcel = async (req, res) => {
  try {
    // Loại trừ các trường password và role từ kết quả truy vấn
    const result = await userModel.find().select("-password -role");

    if (result && result.length > 0) {
      const filePath = "users.xlsx";
      const wb = XLSX.utils.book_new();
      const jsonResult = result.map((user) => {
        const obj = user.toObject();
        obj._id = user._id.toString(); // Add id field explicitly
        return obj;
      });

      const ws = XLSX.utils.json_to_sheet(jsonResult);
      XLSX.utils.book_append_sheet(wb, ws, "Users");
      XLSX.writeFile(wb, filePath);

      res.download(filePath, (err) => {
        if (err) {
          // console.log(err);
        }
        // Xóa file sau khi gửi
        fs.unlinkSync(filePath);
      });
    } else {
      res.status(404).json({ message: "No users found" });
    }
  } catch (error) {
    res.status(500).json({ message: "Failed to fetch data" });
  }
};
//lấy tất cả user
const getAllUser = async (req, res) => {
  const limit = req.query.limit;
  const page = req.query.page;
  const searchContent = req.query.search;
  const isAdmin = req.query.admin || false;
  const isModerator = req.query.moderator || false;
  const isUser = req.query.user || false;
  const condition = { deletedAt: null };
  if (searchContent) {
    // Case-insensitive search for username or email
    condition.$or = [
      { username: { $regex: new RegExp(searchContent, "i") } },
      { email: { $regex: new RegExp(searchContent, "i") } },
    ];

    // Check if searchContent is a valid ObjectId
    if (mongoose.Types.ObjectId.isValid(searchContent)) {
      condition.$or.push({ _id: searchContent });
    }
  }
  let roles = [];

  if (isAdmin === "true") {
    roles.push("admin");
  }
  if (isModerator === "true") {
    roles.push("moderator");
  }
  if (isUser === "true") {
    roles.push("user");
  }

  // If no specific role flags are provided, include all roles
  if (roles.length === 0) {
    roles = ["admin", "moderator", "user"];
  }
  try {
    //lấy tất cả users, ẩn trường password
    const users = await userModel
      .find(condition)
      // .sort({ createdAt: -1 })
      .select("-password")
      .limit(limit)
      .skip((page - 1) * limit)
      .populate({
        path: "roles",
        match: { name: { $in: roles } },
      });
    // Filter users to ensure they have either the 'admin' or 'user' role
    const result = users.filter((user) => {
      return user.roles && user.roles.some((role) => roles.includes(role.name));
    });
    return res.status(200).json({
      message: "Get all user successfully",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Have an error",
    });
  }
};
//lấy user theo id
const getUserById = async (req, res) => {
  const userId = req.params.userId;
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "ID nhập vào ko hợp lệ",
    });
  }
  try {
    const result = await userModel.findById(userId).populate("roles");
    if (result) {
      return res.status(200).json({
        message: "get user by id successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "Id ko hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "have an error",
    });
  }
};
//lấy user theo user name
const getUserByUsername = async (req, res) => {
  const username = req.params.username;
  try {
    const result = await userModel
      .find({ username: username })
      .populate("roles");
    if (result.length > 0) {
      return res.status(200).json({
        message: "get user by username successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "user not found",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "have an error",
    });
  }
};
//ko làm tạo mới, tạo mới thông qua signup
const createUser = (req, res) => {
  res.status(200).json({
    message: "Create User",
  });
};
//hàm update thông tin user dùng cho ai có tk là được
const updateUser = async (req, res) => {
  const userId = req.params.userId;
  const { avatar, password } = req.body;
  try {
    const newItemUpdate = {};
    if (password) {
      //ma hoa password
      newItemUpdate.password = bcrypt.hashSync(password, 8);
    }
    if (avatar) {
      //ma hoa password
      newItemUpdate.avatar = avatar;
    }
    const result = await userModel.findByIdAndUpdate(userId, newItemUpdate, {
      new: true,
    });
    if (result) {
      return res.status(200).json({
        message: "User updated successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID chưa hợp lệ",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Have an error",
    });
  }
};
//hàm chỉnh sửa role, tách khỏi hàm updateUser vì chỉ cho vai trò admin đổi được thôi
const updateRoleUser = async (req, res) => {
  const userId = req.params.userId;
  const { roleId } = req.body;
  try {
    const newItemUpdate = {};
    if (roleId) {
      //vì roles là 1 mảng
      // Tìm người dùng trước để lấy mảng roles hiện tại
      const user = await userModel.findById(userId);
      if (!user) {
        return res.status(400).json({
          message: "User not found",
        });
      }
      // Thêm roleId (la 1 mang) mới vào mảng roles đã có sẵn của user
      newItemUpdate.roles = roleId;
    }
    const result = await userModel.findByIdAndUpdate(userId, newItemUpdate, {
      new: true,
    });
    if (result) {
      return res.status(200).json({
        message: "User Role updated successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "ID not correct",
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Have an error",
    });
  }
};
//xóa mềm user
const softDeleteUser = async (req, res) => {
  const userId = req.params.userId;
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    res.status(400).json({
      message: "ID ko hợp lệ",
    });
  }
  try {
    const user = await userModel.findById(userId);
    if (!user) {
      return res.status(404).json({ message: "user not found" });
    }
    if (user.customer) {
      await user.populate({
        path: "customer",
        populate: {
          path: "orders",
        },
      });
      // Check if any order active
      const hasActiveOrder = user.customer.orders.some(
        (order) => order.status !== "canceled" && order.status !== "received"
      );
      if (hasActiveOrder) {
        return res
          .status(400)
          .json({ message: "Cannot delete user with active orders" });
      }
    }
    // Cập nhật lại deletedAt của orders và orderDetails của người dùng thành null
    await orderModel.updateMany({ userId: userId }, { deletedAt: null });
    await orderDetailModel.updateMany({ userId: userId }, { deletedAt: null });
    //sau đó tiến hành gọi method softDelete trong userModel
    await user.softDelete();
    res.status(200).json({ message: "user soft deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: "Have an error" });
  }
};
//lấy tất cả user đã xóa mềm
const getAllUserDeleted = async (req, res) => {
  try {
    //get product in softDelete (deletedAt !== null)
    const result = await userModel
      .find({ deletedAt: { $ne: null } })
      // .sort({ createdAt: -1 })
      .populate("roles");

    if (result) {
      return res.status(200).json({
        message: "Get all product deleted successfully",
        data: result,
      });
    } else {
      return res.status(400).json({
        message: "product deleted not found",
        data: [],
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: "Have an error",
    });
  }
};
//restore những user đã xóa mềm
const restoreSoftDelete = async (req, res) => {
  const userId = req.params.userId;
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    res.status(400).json({
      message: "ID ko hợp lệ",
    });
  }
  try {
    const userRestored = await userModel.findById(userId);
    if (!userRestored) {
      return res.status(404).json({ message: "user not found" });
    }

    await userRestored.restore();
    res.status(200).json({ message: "user restored successfully" });
  } catch (error) {
    res.status(500).json({ message: "have an error" });
  }
};
module.exports = {
  getAllUser,
  createUser,
  updateUser,
  softDeleteUser,
  getUserById,
  getAllUserDeleted,
  restoreSoftDelete,
  getUserByUsername,
  exportUsersToExcel,
  updateRoleUser,
};
