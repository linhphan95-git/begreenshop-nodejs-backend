const refreshTokenModel = require("../models/refreshToken.model");
const {v4: uuidv4} = require("uuid");

const createToken = async (user) => {
  let expiredAt = new Date();
  expiredAt.setSeconds(
    expiredAt.getSeconds() + process.env.JWT_REFRESH_EXPIRATION
  )
  //tạo reftoken ngẫu nhiên bằng thư viện uuid
  let token = uuidv4();
  let refreshTokenObj = new refreshTokenModel({
    token: token,
    user: user._id, //thêm thông tin user (Đã truyền từ param vào) để định danh user đó là ai
    expiredDate: expiredAt.getTime()
  })
  // sau khi điền xong đủ thì tạo reftoken
  const refreshToken = await refreshTokenObj.save();
  return refreshToken.token;
}

module.exports = {
  createToken
};