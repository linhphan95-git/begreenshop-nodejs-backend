const express = require("express");
const router = express.Router();
const {
  getAllOrderDetail,
  createOrderDetail,
  getOrderDetailById,
  updateOrderDetail,
  deleteOrderDetail,
  getProductsOfOrderDetail,
  getFeaturedProducts,
} = require("../controllers/orderDetail.controller");
const userMiddleWare = require("../middlewares/user.middleware");

//lấy tất cả những orderDetail - cần login role nào cũng được
router.get("/", userMiddleWare.verifyToken, getAllOrderDetail);
//lấy thông tin product chi tiết trong orderDetail thông qua productId - chỉ cần login (role nào cũng được)
router.get(
  "/product/:productId",
  userMiddleWare.verifyToken,
  getProductsOfOrderDetail
);
//tạo orderDetail - cần login role nào cũng được
router.post("/", userMiddleWare.verifyToken, createOrderDetail);
//lây orderDetail theo id - cần login role nào cũng được
router.get("/:orderdetailId", userMiddleWare.verifyToken, getOrderDetailById);
//chỉnh sửa orderDetail - cần login role admin (cd xóa mềm user thì chỉnh sửa cả orders và orderDetail)
router.put(
  "/:orderdetailId",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  updateOrderDetail
);
//tạo orderDetail - cần login role admin (ko dùng vì dùng xóa mềm)
router.delete(
  "/:orderdetailId",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  deleteOrderDetail
);
module.exports = router;
