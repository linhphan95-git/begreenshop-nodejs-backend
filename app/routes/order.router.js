const express = require("express");
const router = express.Router();
const {
  getAllOrder,
  createOrder,
  getOrderById,
  updateOrder,
  deleteOrder,
  getOrderInDay,
  restoreSoftDeleteOrder,
  getAllOrdersDeleted,
  softDeleteOrder,
  exportOrdersToExcel,
  getOrdersOfCustomer,
} = require("../controllers/order.controller");
const userMiddleWare = require("../middlewares/user.middleware");

//lấy tất cả orders của 1 customer theo customerID - chỉ cần login
router.get(
  "/customerOrders/",
  // [userMiddleWare.verifyToken],
  getOrdersOfCustomer
);
//xem download users table (phải có quyền admin/moderator)
router.get(
  "/export/",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  exportOrdersToExcel
);
//xem tất cả những orders đã bị xóa
router.get(
  "/deleted/",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  getAllOrdersDeleted
);
//lấy những order trong ngày - login và roles admin/moderator
router.get(
  "/latest",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  getOrderInDay
);

//lấy tất cả orders - cần có roles admin/moderator
router.get(
  "/",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  getAllOrder
);
//tạo order mới - chỉ cần login
router.post("/", [userMiddleWare.verifyToken], createOrder);
//lấy order theo id - chỉ cần login
router.get("/:orderid", [userMiddleWare.verifyToken], getOrderById);
//chỉnh sửa order chi tiết - cần login
router.put("/:orderid", [userMiddleWare.verifyToken], updateOrder);
//xóa cứng order- ko dùng nữa
router.delete("/:orderid", deleteOrder);
//Xóa order chi tiết - cần login và quyền admin
router.delete(
  "/softDelete/:orderid",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  softDeleteOrder
);
//Restore order đã xóa mềm
router.put(
  "/restore/:orderid",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  restoreSoftDeleteOrder
);
module.exports = router;
