const express = require("express");
const router = express.Router();
const {
  getAllCustomer,
  createCustomer,
  getCustomerById,
  updateCustomer,
  deleteCustomer,
  getCustomerByEmail,
  exportCustomersToExcel,
} = require("../controllers/customer.controller");
const userMiddleWare = require("../middlewares/user.middleware");

//xuất bảng customers ra file excel
router.get(
  "/export",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  exportCustomersToExcel
);
//get customer detail by email
router.get("/detail/:email", [userMiddleWare.verifyToken], getCustomerByEmail);
//lấy tất cả customers trong bảng- cần login và roles admin/moderator
router.get(
  "/",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  getAllCustomer
);
//tạo customer mới khi order được tạo cho user mới - chỉ cần login
router.post("/", [userMiddleWare.verifyToken], createCustomer);
//lấy customer chi tiết
router.get("/:customerid", [userMiddleWare.verifyToken], getCustomerById);
//chỉnh sửa thông tin customer chi tiết - chỉ cần login
router.put("/:customerid", [userMiddleWare.verifyToken], updateCustomer);
//xóa customer - ko sử dụng nữa
router.delete(
  "/:customerid",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  deleteCustomer
);
module.exports = router;
