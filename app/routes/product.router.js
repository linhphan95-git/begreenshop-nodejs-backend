const express = require("express");
const router = express.Router();

const {
  getAllProduct,
  createProduct,
  getProductById,
  updateProduct,
  deleteProduct,
  getProductsOfType,
  softDeleteProduct,
  restoreSoftDelete,
  getAllProductDeleted,
  getFeaturedProducts,
  exportProductsToExcel,
} = require("../controllers/product.controller");
const userMiddleWare = require("../middlewares/user.middleware");

//xem download products table (phải có quyền admin)
router.get(
  "/export",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  exportProductsToExcel
);
//lấy tất cả các featured products ai cũng xem được ko cần đăng nhập
router.get("/featured", getFeaturedProducts);
//lấy tất cả các products ai cũng xem được ko cần đăng nhập
router.get("/", getAllProduct);
//Xóa 1 product - phải login và có quyền admin/moderator
router.get(
  "/deleted",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  getAllProductDeleted
);
//Tạo mới 1 product - phải login và có quyền admin/moderator
router.post(
  "/",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  createProduct
);
//xem thông tin 1 product - ai cũng xem được ko cần đăng nhập
router.get("/:productid", getProductById);
//edit 1 product - phải login và có quyền admin/moderator
router.put(
  "/:productid",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  updateProduct
);
//delete 1 product - phải login và có quyền admin/moderator
router.delete(
  "/:productid",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  deleteProduct
);
//Xóa mềm 1 product - phải login và có quyền admin/moderator
router.delete(
  "/softDelete/:productid",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  softDeleteProduct
);
//Restore 1 product đã xóa mềm - phải login và có quyền admin/moderator
router.put(
  "/restore/:productid",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  restoreSoftDelete
);
//lấy những sp theo typeId - ko cần login và quyền
router.get("/:productTypeId/types", getProductsOfType);

module.exports = router;
