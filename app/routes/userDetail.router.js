const express = require("express");
const router = express.Router();
const {
 createUserDetail
} = require("../controllers/userDetail.controller");

// router.get('/',getAllCustomer);
router.post('/',createUserDetail);
// router.get('/:customerid',getCustomerById);
// router.put('/:customerid',updateCustomer);
// router.delete('/:customerid',deleteCustomer);
module.exports = router;