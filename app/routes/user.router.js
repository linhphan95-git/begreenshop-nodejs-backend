const express = require("express");

const route = express.Router();

const userMiddleWare = require("../middlewares/user.middleware");
const userController = require("../controllers/user.controller");

//xem download users table (phải có quyền admin/moderator)
route.get(
  "/export/",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  userController.exportUsersToExcel
);
//xem danh sach cac user da xoa (phải có quyền admin)
route.get(
  "/deleted/",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  userController.getAllUserDeleted
);
//lấy danh sách tất cả user (phải có quyền admin/moderator)
route.get(
  "/",
  [userMiddleWare.verifyToken, userMiddleWare.IsNotUser],
  userController.getAllUser
);
//lay 1 user theo id (cần đăng nhập, quyền nào cũng được)
route.get("/:userId", [userMiddleWare.verifyToken], userController.getUserById);
//lấy 1 user theo username(cần đăng nhập, quyền nào cũng được)
route.get(
  "/username/:username",
  [userMiddleWare.verifyToken],
  userController.getUserByUsername
);

//tạo 1 user mới
// route.post("/", [userMiddleWare.verifyToken, userMiddleWare.checkUser], userController.createUser);
// sửa thông tin role 1 user - quyền admin
route.put(
  "/roles/:userId",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  userController.updateRoleUser
);
//sửa thông password/avatar của 1 user (cần đăng nhập, quyền nào cũng được)
route.put("/:userId", [userMiddleWare.verifyToken], userController.updateUser);
//xóa 1 user (xóa mềm) (phải có quyền admin)
route.delete(
  "/softDelete/:userId",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  userController.softDeleteUser
);
//restore 1 user đã xóa mềm (phải có quyền admin)
route.put(
  "/restore/:userId",
  [userMiddleWare.verifyToken, userMiddleWare.checkAdmin],
  userController.restoreSoftDelete
);

module.exports = route;
