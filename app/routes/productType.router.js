const express = require("express");
const router = express.Router();
const {
  getAllProductType,
  createProductType,
  getProductTypeByID,
  updateProductType,
  deleteProductType,
  softDeleteTypeProduct,
  getAllTypesDeleted,
  restoreSoftDeleteType,
} = require("../controllers/productType.controller");

router.get("/typesDeleted", getAllTypesDeleted);
router.get("/", getAllProductType);
router.post("/", createProductType);
router.get("/:typeId", getProductTypeByID);
router.put("/restore/:typeId", restoreSoftDeleteType);
router.put("/:typeId", updateProductType);
router.delete("/softDelete/:typeId", softDeleteTypeProduct);
router.delete("/:typeId", deleteProductType);

module.exports = router;
