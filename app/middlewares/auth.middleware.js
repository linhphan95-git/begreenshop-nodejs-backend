const db = require("../models");
const jwt = require("jsonwebtoken");

const User = db.user;
const Role = db.role;
const ROLES = db.ROLES;
//kiểm tra username có bị trùng ko
const checkDuplicateUsername = async (req, res, next) => {
  try {
    const condition = {};
    condition.$or = [
      { username: req.body.username },
      { email: req.body.email },
    ];
    //nếu username hoặc email đã từng tồn tại
    const existUser = await User.findOne(condition);

    if (existUser) {
      res.status(400).send({
        message: "Account is already in use",
      });
      return;
    }

    next();
  } catch (error) {
    process.exit();
  }
};
module.exports = {
  checkDuplicateUsername,
};
