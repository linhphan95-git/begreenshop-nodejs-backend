const db = require("../models");
const jwt = require("jsonwebtoken");

const User = db.user;
const Role = db.role;

//kiểm tra đã login hay chưa?
const verifyToken = async (req, res, next) => {
  try {
    let token = req.headers["x-access-token"];
    if (!token) {
      return res.status(401).send({
        message: "x-access-token not found!",
      });
    }

    const secretKey = process.env.JWT_SECRET_KEY;

    const verified = await jwt.verify(token, secretKey);
    if (!verified) {
      return res.status(401).send({
        message: "x-access-token invalid!",
      });
    }

    const user = await User.findById(verified.id).populate("roles");
    req.user = user;
  } catch (error) {
    if (error.name === "TokenExpiredError") {
      return res.status(401).send({
        message: "TokenExpiredError",
      });
    }
    return res.status(500).send({
      message: "Internal server error!",
    });
  }

  next();
};

//kiểm tra role có phải là admin ko
const checkAdmin = (req, res, next) => {
  const userRoles = req.user.roles;
  if (userRoles) {
    for (let i = 0; i < userRoles.length; i++) {
      if (userRoles[i].name == "admin") {
        next();
        return;
      }
    }
  }
  return res.status(401).send({
    message: "Bạn không có quyền truy cập!",
  });
};
//kiểm tra role có phải là moderator hoặc admin ko - nếu user thì ko đc
const IsNotUser = (req, res, next) => {
  const userRoles = req.user.roles;
  if (userRoles) {
    for (let i = 0; i < userRoles.length; i++) {
      if (userRoles[i].name !== "user") {
        next();
        return;
      }
    }
  }

  return res.status(401).send({
    message: "Admin and Moderator Unauthorized!",
  });
};
module.exports = {
  verifyToken,
  checkAdmin,
  IsNotUser,
};
